﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Etat_Partie : MonoBehaviour {

	// Etats de la machine a états
	public enum Etat {Debut_Partie,
		Debut_Tour_Joueur,
		Lance_Dé,
		MouvVoiture,
		Verif_2000,
		Nuit_Hotel,
		Encheres,
		Type_Case,
		Achat_Terrain,
		Construction_Gratuite,
		Achat_Entrees,
		Construction,
		Fin_Tour_Joueur,
		Fin_Partie}
			public static Etat etat;

	// Etats de transition
	public struct Transition_State {
		public bool isTransitionDebutPartie;
		public bool isTransitionDebutTourJoueur;
		public bool isTransitionLancerDe;
		public bool isTransitionMouvVoiture;
		public bool isTransition2000euros;
		public bool isTransitionNuitHotel;
		public bool isTransitionType_case;
		public bool isTransitionEncheres;
		public bool isTransitionAchatTerrain;
		public bool isTransitionConstructionGratuite;
		public bool isTransitionAchatEntrees;
		public bool isTransitionConstruction;
		public bool isTransitionFinTourJoueur;
		public bool isTransitionFinPartie;
	}
	public static Transition_State transitionState;

	// Sous etat achat entree
	public enum Ss_Etat_Achat_Entree
	{
		Non_Applicable,
		Choix_Hotel,
		Paiement,
		Placer_Entree,
		Fin_Ss_Etat
	}
	public static Ss_Etat_Achat_Entree ss_Etat_Achat_Entree;
	public struct Ss_Achat_Entree_Transition
	{
		public bool isTransitionChoixHotel;
		public bool isPaiement;
		public bool isPlacementEntree;
	}
	public static Ss_Achat_Entree_Transition transitionStateAchatEntree;


	// Sous etat achat terrain
	public enum Ss_Etat_Achat_Terrain
	{
		Non_Applicable,
		Choix_Terrain,
		Paiement,
		Fin_Ss_Etat
	}
	public static Ss_Etat_Achat_Terrain ss_Etat_Achat_Terrain;
	public struct Ss_Achat_Terrain_Transition
	{
		public bool isTransitionChoixTerrain;
		public bool isPaiement;
	}
	public static Ss_Achat_Terrain_Transition transitionStateAchatTerrain;

	// Sous etat nuit hotel
	public enum Ss_Etat_Nuit_Hotel{
		Lancer_Dé,
		Paiement,
		Fin_Ss_Etat
	}
	public static Ss_Etat_Nuit_Hotel ss_Etat_Nuit_Hotel;

	// Use this for initialization
	void Start () {
		Reset_Ss_Etat_Nuit_Hotel ();
		Reset_Ss_Etat_Achat_Entree ();
		Reset_Ss_Etat_Achat_Terrain ();
		SetZeroTransition ();
		To_Debut_Partie ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void SetZeroTransition()
	{
		transitionState.isTransitionDebutPartie = false;
		transitionState.isTransitionDebutTourJoueur = false;
		transitionState.isTransitionLancerDe = false;
		transitionState.isTransitionMouvVoiture = false;
		transitionState.isTransition2000euros = false;
		transitionState.isTransitionNuitHotel = false;
		transitionState.isTransitionEncheres = false;
		transitionState.isTransitionAchatTerrain = false;
		transitionState.isTransitionConstructionGratuite = false;
		transitionState.isTransitionAchatEntrees = false;
		transitionState.isTransitionType_case = false;
		transitionState.isTransitionFinTourJoueur = false;
		transitionState.isTransitionFinPartie = false;
	}

	public static void To_Debut_Partie()
	{
		Debug.Log ("Debut : Debut Partie");
		etat = Etat.Debut_Partie;
		SetZeroTransition ();
		transitionState.isTransitionDebutPartie = true;
		Debug.Log ("Debut : Debut Partie");
	}

	public static void To_Debut_Tour_Joueur()
	{
		Debug.Log ("Debut : Debut tour joueur");
		if ((etat == Etat.Fin_Tour_Joueur) | (etat == Etat.Debut_Partie)) {
			etat = Etat.Debut_Tour_Joueur;
			SetZeroTransition ();
			transitionState.isTransitionDebutTourJoueur = true;
			Debug.Log ("Debut : Debut tour joueur");
		}
	}

	public static void To_Lance_De()
	{
		Debug.Log ("Debut : Lance dé");
		if (etat == Etat.Debut_Tour_Joueur) {
			etat = Etat.Lance_Dé;
			SetZeroTransition ();
			transitionState.isTransitionLancerDe = true;
			Debug.Log ("Fin : Lance dé");
		}

	}

	public static void To_Mouv_Voiture()
	{
		Debug.Log ("Debut : Mouv Voiture");
		if (etat == Etat.Lance_Dé) {
			etat = Etat.MouvVoiture;
			SetZeroTransition ();
			transitionState.isTransitionMouvVoiture = true;
			Debug.Log ("Fin : Mouv Voiture");
		}

	}

	public static void To_Verif2000euros()
	{
		Debug.Log ("Debut : Verif 2000 euros");
		if (etat == Etat.MouvVoiture) {
			etat = Etat.Verif_2000;
			SetZeroTransition ();
			transitionState.isTransition2000euros = true;
			Debug.Log ("Fin :Verif 2000 euros");
		}

	}

	public static void To_NuitHotel()
	{
		Debug.Log ("Debut : Nuit Hotel");
		if (etat == Etat.Verif_2000) {
			etat = Etat.Nuit_Hotel;
			SetZeroTransition ();
			transitionState.isTransitionNuitHotel = true;
			Debug.Log ("Fin : Nuit Hotel");
		}

	}
		
	public static void To_Encheres()
	{
		Debug.Log ("Debut : Encheres");
		if (etat == Etat.Nuit_Hotel) {
			etat = Etat.Encheres;
			SetZeroTransition ();
			transitionState.isTransitionEncheres = true;
			Debug.Log ("Fin : Encheres");
		}

	}

	public static void To_AchatTerrain()
	{
		Debug.Log ("Debut : Achat Terrain");
		if (etat == Etat.Type_Case) {
			etat = Etat.Achat_Terrain;
			SetZeroTransition ();
			transitionState.isTransitionAchatTerrain = true;
			Debug.Log ("Fin : Achat Terrain");
		}

	}

	public static void To_ConstructionGratuite()
	{
		Debug.Log ("Debut : Construction Gratuite");
		if (etat == Etat.Type_Case) {
			etat = Etat.Construction_Gratuite;
			SetZeroTransition ();
			transitionState.isTransitionConstructionGratuite = true;
			Debug.Log ("Fin : Construction Gratuite");
		}

	}

	public static void To_Construction()
	{
		Debug.Log ("Debut : Construction");
		if (etat == Etat.Type_Case) {
			etat = Etat.Construction;
			SetZeroTransition ();
			transitionState.isTransitionConstruction = true;
			Debug.Log ("Fin : Construction");
		}

	}

	public static void To_AchatEntrees()
	{
		Debug.Log ("Debut : Achat Entrees");
		if (etat == Etat.Nuit_Hotel) {
			etat = Etat.Achat_Entrees;
			SetZeroTransition ();
			transitionState.isTransitionAchatEntrees = true;
			Debug.Log ("Fin : Achat Entrees");
		}

	}

	public static void To_Type_Case()
	{
		Debug.Log ("Debut : Type case");
		if (etat == Etat.Achat_Entrees) {
			etat = Etat.Type_Case;
			SetZeroTransition ();
			transitionState.isTransitionType_case = true;
			Debug.Log ("Fin : Type case");
		}

	}

	public static void To_Fin_Tour_Joueur()
	{
		Debug.Log ("Debut : Fin Tour joueur");
		if (etat == Etat.Lance_Dé) {
			etat = Etat.Fin_Tour_Joueur;
			SetZeroTransition ();
			transitionState.isTransitionFinTourJoueur = true;
			Debug.Log ("Fin : Fin Tour joueur");
		}
	}
		
	public static void To_Fin_Partie()
	{
		Debug.Log ("Debut : Fin Partie");
		if (etat == Etat.Fin_Tour_Joueur) {
			etat = Etat.Fin_Partie;
			SetZeroTransition ();
			transitionState.isTransitionFinPartie = true;
			Debug.Log ("Fin : Fin Partie");
		}
	}

	public static void To_AchatEntree_ChoixHotel ()
	{
		ss_Etat_Achat_Entree = Ss_Etat_Achat_Entree.Choix_Hotel;
		transitionStateAchatEntree.isTransitionChoixHotel = true;
	}

	public static void To_AchatEntree_Paiement ()
	{
		ss_Etat_Achat_Entree = Ss_Etat_Achat_Entree.Paiement;
		transitionStateAchatEntree.isPaiement = true;
	}

	public static void To_AchatEntree_PlacementEntree ()
	{
		ss_Etat_Achat_Entree = Ss_Etat_Achat_Entree.Placer_Entree;
		transitionStateAchatEntree.isPlacementEntree = true;
	}

	public static void To_AchatTerrain_ChoixTerrain ()
	{
		ss_Etat_Achat_Terrain = Ss_Etat_Achat_Terrain.Choix_Terrain;
		transitionStateAchatTerrain.isTransitionChoixTerrain = true;
	}

	public static void To_AchatTerrain_Paiement ()
	{
		ss_Etat_Achat_Terrain = Ss_Etat_Achat_Terrain.Paiement;
		transitionStateAchatTerrain.isPaiement = true;
	}

	// Mise à la valeur par défaut du sous état de l'achat des entrées
	public static void Reset_Ss_Etat_Achat_Entree()
	{
		ss_Etat_Achat_Entree = Ss_Etat_Achat_Entree.Non_Applicable;
		transitionStateAchatEntree.isTransitionChoixHotel = false;
		transitionStateAchatEntree.isPlacementEntree = false;
		transitionStateAchatEntree.isPaiement = false;
	}

	// Mise à la valeur par défaut du sous état de l'achat des terrains
	public static void Reset_Ss_Etat_Achat_Terrain()
	{
		ss_Etat_Achat_Terrain = Ss_Etat_Achat_Terrain.Non_Applicable;
		transitionStateAchatTerrain.isTransitionChoixTerrain = false;
		transitionStateAchatTerrain.isPaiement = false;
	}

	// Mise à la valeur par défaut du sous état des nuits d'hotel
	public static void Reset_Ss_Etat_Nuit_Hotel()
	{
		ss_Etat_Nuit_Hotel = Ss_Etat_Nuit_Hotel.Lancer_Dé;
	}

}
