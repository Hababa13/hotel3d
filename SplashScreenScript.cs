﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreenScript : MonoBehaviour {
	private float T0;
	float C_Temps_Affichage = 3.0f;

	// Use this for initialization
	void Start () {
		string[] strs = Input.GetJoystickNames ();
		foreach (string st in strs)
			Debug.Log (st);
		T0 = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time - T0 > C_Temps_Affichage)
		{
			SceneManager.LoadScene("Menu");
		}
	}
}
