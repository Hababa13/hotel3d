﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lance : MonoBehaviour {

	public GameObject g;
	public static bool valeur = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.A))
		{
			valeur = true;
			g.GetComponent<Rigidbody> ().AddForce (new Vector3 (0.0f,Random.Range(40.0f, 60.0f), 0.0f), ForceMode.Impulse);	
			g.GetComponent<Rigidbody> ().angularVelocity = Random.onUnitSphere*Random.Range(6*6, 8*6);	
		}
		if (!isRolling (g) & valeur) {
/*			Debug.Log ("Position = " + g.GetComponent<Transform> ().localPosition);
			Debug.Log (g.transform.TransformVector (Vector3.up) + " "+ Vector3.Distance (g.transform.TransformVector (Vector3.up), Vector3.up));
			Debug.Log (g.transform.TransformVector (Vector3.down) + " "+ Vector3.Distance (g.transform.TransformVector (Vector3.down), Vector3.up));
			Debug.Log (g.transform.TransformVector (Vector3.forward) + " "+ Vector3.Distance (g.transform.TransformVector (Vector3.forward), Vector3.up));
			Debug.Log (g.transform.TransformVector (Vector3.back) + " "+ Vector3.Distance (g.transform.TransformVector (Vector3.back), Vector3.up));
			Debug.Log (g.transform.TransformVector (Vector3.left) + " "+ Vector3.Distance (g.transform.TransformVector (Vector3.left), Vector3.up));
			Debug.Log (g.transform.TransformVector (Vector3.right) + " "+ Vector3.Distance (g.transform.TransformVector (Vector3.right), Vector3.up));*/
			if (Vector3.Distance (g.transform.TransformVector (Vector3.up), Vector3.up) < 0.01f)
				Debug.Log ("TOMBE SUR : H");
			if (Vector3.Distance (g.transform.TransformVector (Vector3.down), Vector3.up) < 0.01f)
				Debug.Log ("TOMBE SUR : 2");
			if (Vector3.Distance (g.transform.TransformVector (Vector3.forward), Vector3.up) < 0.01f)
				Debug.Log ("TOMBE SUR : vert1");
			if (Vector3.Distance (g.transform.TransformVector (Vector3.back), Vector3.up) < 0.01f)
				Debug.Log ("TOMBE SUR : rouge");
			if (Vector3.Distance (g.transform.TransformVector (Vector3.left), Vector3.up) < 0.01f)
				Debug.Log ("TOMBE SUR : vert2");
			if (Vector3.Distance (g.transform.TransformVector (Vector3.right), Vector3.up) < 0.01f)
				Debug.Log ("TOMBE SUR : vert3");
		}
	}

	public bool isRolling(GameObject g)
	{
		if ((g.GetComponent<Rigidbody> ().velocity == Vector3.zero) && (g.GetComponent<Rigidbody> ().angularVelocity == Vector3.zero))
			return false;
		else
			return true;
	}
}
