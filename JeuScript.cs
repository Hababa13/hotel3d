﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class JeuScript : MonoBehaviour {
	public Camera MyCamera;
	public GameObject Plateau;
	public float Force_De = 1.0f;
	public GameObject Crosshair;
	public float Vitesse_Crosshair_H = 10.0f;
	public float Vitesse_Crosshair_V = 10.0f;

	public float Vitesse_Rotation_Plan 	= 10.0f;
	public float Vitesse_Distance 		= 10.0f;

	public GameObject Dé6;
	public GameObject De_Construction;

	// Texte dans le panel des joueurs
	public Text texteJoueur;

	// Texte dans les boites de dialogue
	public Text Dialogue1;
	public Text Dialogue2;

	public Canvas MenuCamera;
	public Canvas Sortie;
	public Canvas MenuTouches;

	public GameObject UneEntree;

	public Text Player1;
	public Text Player2;
	public Text Player3;
	public Text Player4;
	public Text Argent1;
	public Text Argent2;
	public Text Argent3;
	public Text Argent4;
	public Image Image1;
	public Image Image2;
	public Image Image3;
	public Image Image4;

	public GameObject VoitureRouge;
	public GameObject VoitureVerte;
	public GameObject VoitureBleue;
	public GameObject VoitureJaune;

	public GameObject Voiture_Transform;

	public Canvas Canvas_Cartes;

	public Material M_Boomerang;
	public Material M_Fujiyama;
	public Material M_Etoile;
	public Material M_President;
	public Material M_Royal;
	public Material M_Waikiki;
	public Material M_Taj_Mahal;
	public Material M_Safari;

	public static Material[] listeMaterialHotel;

	public static GameObject maVoitureRouge;
	public static GameObject maVoitureVerte;
	public static GameObject maVoitureBleue;
	public static GameObject maVoitureJaune;

	public struct TrigCommandes
	{
		public bool MasqueCommandes;
		public bool LanceDé;
		public bool LanceConstruction;
		public bool VoirProprio;
		public bool Acheter;
		public bool FinTour;
		public bool Quitter;
		public bool CamG;
		public bool CamD;
		public bool CamH;
		public bool CamB;
		public bool CamRapproche;
		public bool CamEloigne;
		public bool CamFocus;
	}

	public static TrigCommandes trigCommandes;


	// Données liées aux états
	private const float C_Tempo_debut_partie  = 3.0f;
	private const float C_Tempo_debut_joueur  = 3.0f;
	private const float C_Tempo_fin_partie  = 3.0f;
	private const float C_Tempo_2000_euros  = 3.0f;
	private float T0_Debut_partie;
	private float T0_Debut_Tour_Joueur;
	private float T0_Fin_Partie;
	private float T0_Debut_Mouv_Voiture;
	private float T0_Debut_2000_euros;
	private float Abscisse_Curv_T;
	private const float Speed_Voiture = 1.0f;
	private const int Case_2000 = 6;
	private const int Case_Achat_Entrees = 25;
	private bool isPayerProprio = false;
	private bool isJoueurDoitPayerProprio = false;
	private bool isPossibleEntree = false;
	private Plateau_Jeu.Hotel_Nom nomHotelPayer;
	private bool is2000euros = false;
	private struct ImpactPlateau {
		public bool touche;
		public Vector3 PointPlateau;
	}
	private ImpactPlateau impactPlateau;

	public enum Choix_Dialogue {Pas_Choisi, Oui, Non}
	public static Choix_Dialogue choixDialogue = Choix_Dialogue.Pas_Choisi;

	// Les Voitures
	public struct InfoVoitures
	{
		public static GameObject[] AllVoitures;
		public static GameObject[] AllPrefabVoitures;
		public static Vector3[] AllPositionDepart;
		public static Quaternion quaternionDepart;
	}

	public static InfoVoitures infoVoitures;

	// Panel des joueurs
	public struct PanelJoueurs
	{
		public static Text[] AllPlayers;
		public static Text[] AllArgent;
		public static Image[] AllImage;
	}

	// Info de la camera
	public struct InfoCamera
	{
		public static float Vitesse_Rotation_Plan;
		public static float Vitesse_Distance;
		public static Vector3 PositionCamera;
		public static float Theta;
		public static float Phi;
		public static float Distance;
		public static bool isCameraCenteredOnCar;
		public static Vector3 DecalageCameraPlateau;
		public static Vector3 DecalageCameraVoiture;
	}
		
	static Vector3 VecteurCamera;

	public struct ListeAxes
	{
		// Mouvement camera
		public string Tourne_Droite_Gauche;
		public string Tourne_Haut_Bas;
		public string Distance;
		public string CrossHair_H;
		public string CrossHair_V;
		public string Bouton_Valid;

		// Change le point de vue de la camera
		public string Change_Focus;

		// Commandes diverses
		public string Acheter;
		public string Voir_Cartes;
		public string Defilement_Cartes;

		// Commande de lancer de dés
		public string Lance_De6;
		public string Lance_De_Construction;
		public string Affiche_Masque_Commandes;

		// Fin de tour/jeu
		public string Fin_Tour;
		public string Quitter;
	}

	public static ListeAxes listeAxes;

	public static bool isDisplayedCommands;

	public enum Valeur_De_Construction {Construction_Gratuire, Double, Vert, Rouge}

	public struct Des
	{
		public int De;
		public Valeur_De_Construction De_Construction;
		public bool isVisible_De;
		public bool isVisible_De_Construction;
		public Vector3 De_position;
		public Vector3 De_Construction_Position;
		public bool Dé6_Lancé;
		public bool Dé_Construction_Lancé;
		public int Case_courante_Joueur;
		public int Prochaine_Case;
		public int Nombre_Nuits;
	}

	public static Des De;

	public struct Cartes
	{
		public bool isDisplayedCartes;
		public bool alreadyPressed;
		public Plateau_Jeu.Hotel_Nom hotelCourant;
	}

	public static Cartes cartesHotel;

	// Use this for initialization
	void Start () {
		Joueur.DebutDePartie ();

		// Active les boutons
		MenuCamera.gameObject.SetActive(true);
		Sortie.gameObject.SetActive(true);
		MenuTouches.gameObject.SetActive(true);

		// Set du point d'impact plateau
		impactPlateau.touche = false;
		impactPlateau.PointPlateau = Vector3.zero;

		// Met le trigger des commandes par crosshair à zero
		ResetTriggerCommandes();

		// Affichage des commandes
		isDisplayedCommands = true;

		// Affichage des cartes
		cartesHotel.isDisplayedCartes = true;
		cartesHotel.alreadyPressed = false;

		listeMaterialHotel = new Material[8] {
			M_Boomerang,
			M_Fujiyama,
			M_Etoile,
			M_President,
			M_Royal,
			M_Waikiki,
			M_Taj_Mahal,
			M_Safari
		};

		Affiche_Cartes ();

		// Cache les menus de dialogue
		AfficheDialogue("","",false);

		//Initialisation des dés
		Random.InitState((int)Mathf.Ceil(Time.time));

		De.De_position = new Vector3 (-36.0f, 2.0f, -20.0f);
		De.De_Construction_Position = new Vector3 (-26.0f, 2.0f, -20.0f);
		De.isVisible_De = false;
		De.isVisible_De_Construction = false;
		De.Dé6_Lancé = false;
		De.Dé_Construction_Lancé = false;
		De.Case_courante_Joueur = 0;
		De.Prochaine_Case = 0;
		Affiche_Des ();

		// Initialisation des commandes
		SetListeAxes("Horizontal", // Tourne Droite/Gauche
			"Vertical",				// Tourne Haut/Bas
			"Distance",				// Axe Distance
			"CrossHair_H",			// CrossHair Horizontal
			"CrossHair_V",			// CrossHair Vertical
			"Fire1",				// Bouton Validation
			"Focus",				// Axe focus
			"Acheter",				// Acheter
			"Voir_Cartes",			// Voir les cartes de propriete
			"Defilement_cartes",	// Defilement des cartes
			"Lance_De6",			// Lance Dé 6
			"Lance_De_Construction",	// Lance_Dé construction
			"Affiche_Masque_Commandes", // Affiche ou masque les commandes
			"Fin_Tour",				// Fin de tour
			"Quitter");				// Quitter le jeu

		// Position initiale camera
		InfoCamera.Vitesse_Rotation_Plan = Vitesse_Rotation_Plan;
		InfoCamera.Vitesse_Distance = Vitesse_Distance;
		InfoCamera.Theta = 0.0f;
		InfoCamera.Phi = 0.0f;
		InfoCamera.Distance = 0.0f;
		InfoCamera.DecalageCameraPlateau = new Vector3 (-90.0f, 45.0f, 150.0f); // Theta, Phi, Distance
		InfoCamera.DecalageCameraVoiture = new Vector3 (-90.0f, 20.0f, 10.0f); // Theta, Phi, Distance
		InfoCamera.isCameraCenteredOnCar = false;
		SetCameraInfo (InfoCamera.DecalageCameraPlateau.x,
			InfoCamera.DecalageCameraPlateau.y,
			InfoCamera.DecalageCameraPlateau.z,
			false,  // Referentiel camera sur plateau
			true); // La vue a changé => Permet d'initialiser la vue à la position par défaut

		JeuScript.PanelJoueurs.AllPlayers = new Text[4] { Player1, Player2, Player3, Player4 };
		JeuScript.PanelJoueurs.AllArgent = new Text[4] { Argent1, Argent2, Argent3, Argent4 };
		JeuScript.PanelJoueurs.AllImage = new Image[4] { Image1, Image2, Image3, Image4 };

		JeuScript.InfoVoitures.AllVoitures = new GameObject[4] { maVoitureRouge, maVoitureVerte, maVoitureBleue, maVoitureJaune };
		JeuScript.InfoVoitures.AllPrefabVoitures = new GameObject[4] { VoitureRouge, VoitureVerte, VoitureBleue, VoitureJaune };
		JeuScript.InfoVoitures.AllPositionDepart = new Vector3[4] { 
			new Vector3(-87.06f, 0.5f, -19.76f), 
			new Vector3(-75.94f, 0.5f, -19.89f),
			new Vector3(-81.5f, 0.5f, -19.94f),
			new Vector3(-70.22f, 0.5f, -20.03f) };
		JeuScript.InfoVoitures.quaternionDepart = Quaternion.identity;

		for (int i=0; i<Joueur.nb_joueurs ; i++) 
		{
			JeuScript.PanelJoueurs.AllPlayers[i].text = Joueur.joueurs [i].nomJoueur;
		}

		// Le premier joueur joue
		Joueur.SetEtatJoueur(0,Joueur.Etat.Actif);

		// Creation des voitures
		GameObject voiture;
		foreach (Joueur.UnJoueur monjoueur in Joueur.joueurs) {
			voiture = Instantiate (InfoVoitures.AllPrefabVoitures [(int)monjoueur.couleur], 
				InfoVoitures.AllPositionDepart [(int)monjoueur.couleur],
				InfoVoitures.quaternionDepart);
			voiture.transform.SetParent (Voiture_Transform.transform);
			JeuScript.InfoVoitures.AllVoitures [(int)monjoueur.couleur] = voiture;
		}
			
		// Initialisation des timers de changement d'état
		T0_Debut_partie = Time.time;
		T0_Debut_Tour_Joueur = 0.0f;
		T0_Fin_Partie = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		UpdatePanelJoueurs ();
		UpdateCameraControl ();
		UpdateCrosshairControl ();
		GereCommandes ();
		Gere_Machine_Etat ();
	}

	// Mise à jour de la camera
	void UpdateCameraControl()
	{
		float delta_rotation_plan = (Input.GetAxis(listeAxes.Tourne_Droite_Gauche) + (trigCommandes.CamD==true?1:0) - (trigCommandes.CamG==true?1:0))* Vitesse_Rotation_Plan * Time.deltaTime;
		float delta_rotation_vert = (Input.GetAxis(listeAxes.Tourne_Haut_Bas) + (trigCommandes.CamH==true?1:0) - (trigCommandes.CamB==true?1:0)) * Vitesse_Rotation_Plan * Time.deltaTime;
		float delta_distancejeu	= Mathf.Min((Input.GetAxis(listeAxes.Distance)  *(1+ 10* ((trigCommandes.CamEloigne==true?1:0) - (trigCommandes.CamRapproche==true?1:0))))* Vitesse_Distance * Time.deltaTime,460.0f);

		trigCommandes.CamD = false;
		trigCommandes.CamG = false;
		trigCommandes.CamH = false;
		trigCommandes.CamB = false;
		trigCommandes.CamEloigne = false;
		trigCommandes.CamRapproche = false;

		bool referentielCamera = InfoCamera.isCameraCenteredOnCar;
		bool hasChanged = false;
		if (Input.GetButtonDown (listeAxes.Change_Focus) | trigCommandes.CamFocus) {
			trigCommandes.CamFocus = false;
			referentielCamera = !InfoCamera.isCameraCenteredOnCar;
			hasChanged = true;
		}
		SetCameraInfo (delta_rotation_plan, delta_rotation_vert, delta_distancejeu, referentielCamera, hasChanged);
	}

	// Setter sur les infos camera
	void SetCameraInfo(float dtheta, float dphi, float ddistance, bool referentielCamOnCar, bool hasChanged)
	{
		Vector3 Decalage_Courant;
		InfoCamera.Theta += dtheta;
		InfoCamera.Phi += dphi;
		InfoCamera.Distance += ddistance;
		InfoCamera.isCameraCenteredOnCar = referentielCamOnCar;

		// Mise à jour position camera
		if (referentielCamOnCar)  // Si la camera est sur la voiture
		{
			Decalage_Courant = JeuScript.InfoVoitures.AllVoitures [(int)Joueur.joueurs[Joueur.GetJoueurActif()].couleur].GetComponent<Transform> ().position;
			if (hasChanged) { // Si on a changé de vue
			InfoCamera.Theta = InfoCamera.DecalageCameraVoiture.x;
			InfoCamera.Phi = InfoCamera.DecalageCameraVoiture.y;
			InfoCamera.Distance = InfoCamera.DecalageCameraVoiture.z;
			}
				
		}
		else  // Si la camera est sur le plateau
		{
			Decalage_Courant = Plateau.GetComponent<Transform>().position;
			if (hasChanged) { // Si on a changé de vue
			InfoCamera.Theta = InfoCamera.DecalageCameraPlateau.x;
			InfoCamera.Phi = InfoCamera.DecalageCameraPlateau.y;
			InfoCamera.Distance = InfoCamera.DecalageCameraPlateau.z;
			}
		}
		VecteurCamera = InfoCamera.Distance * new Vector3 (Mathf.Cos (Mathf.Deg2Rad * InfoCamera.Theta) * Mathf.Cos(Mathf.Deg2Rad* InfoCamera.Phi),
		Mathf.Sin (Mathf.Deg2Rad * InfoCamera.Phi),
			Mathf.Sin (Mathf.Deg2Rad * InfoCamera.Theta) * Mathf.Cos(Mathf.Deg2Rad* InfoCamera.Phi));
	
		MyCamera.GetComponent<Transform> ().position = VecteurCamera + Decalage_Courant;
		MyCamera.GetComponent<Transform> ().LookAt(Decalage_Courant);
	}

	// Mise à jour du panel des joueurs
	void UpdatePanelJoueurs()
	{
		// Rafraichit le tableau de scores
		for (int i=0; i<Joueur.nb_joueurs ; i++) 
		{
			JeuScript.PanelJoueurs.AllArgent [i].text = Joueur.joueurs [i].argent.ToString();
			JeuScript.PanelJoueurs.AllImage [i].enabled = (Joueur.joueurs [i].etat == Joueur.Etat.Actif);

		}
	}

	// Setter des Controles des axes
	public static void SetListeAxes(string Tourne_Droite_Gauche, string Tourne_Haut_Bas, string Distance, string CrossHair_H, string CrossHair_V,
		string Bouton_Valid, string Change_Focus, string Acheter, string Voir_Cartes, string Defilement_Cartes, string Lance_De6, string Lance_De_Construction,
		string Affiche_Masque_Commandes, string Fin_Tour, string Quitter)
	{
		listeAxes.Tourne_Droite_Gauche = Tourne_Droite_Gauche;
		listeAxes.Tourne_Haut_Bas = Tourne_Haut_Bas;
		listeAxes.Distance = Distance;
		listeAxes.CrossHair_H = CrossHair_H;
		listeAxes.CrossHair_V = CrossHair_V;
		listeAxes.Bouton_Valid = Bouton_Valid;
		listeAxes.Change_Focus = Change_Focus;
		listeAxes.Acheter = Acheter;
		listeAxes.Voir_Cartes = Voir_Cartes;
		listeAxes.Defilement_Cartes = Defilement_Cartes;
		listeAxes.Lance_De6 = Lance_De6;
		listeAxes.Lance_De_Construction = Lance_De_Construction;
		listeAxes.Affiche_Masque_Commandes = Affiche_Masque_Commandes;
		listeAxes.Fin_Tour = Fin_Tour;
		listeAxes.Quitter = Quitter;
	}

	// Gère les commandes
	public void GereCommandes()
	{
		if (Input.GetButtonDown (listeAxes.Affiche_Masque_Commandes) | trigCommandes.MasqueCommandes) {
			trigCommandes.MasqueCommandes = false;
			Affiche_Masque_Commandes ();
		}
		
		if ((Input.GetAxis(listeAxes.Quitter) > 0.2f) | trigCommandes.Quitter)
			OnQuit();

		if (Input.GetButtonDown (listeAxes.Voir_Cartes) | trigCommandes.VoirProprio) {
			trigCommandes.VoirProprio = false;
			Affiche_Cartes ();
		}
		
		if ((Mathf.Abs(Input.GetAxis (listeAxes.Defilement_Cartes)) > 0.001f) & !cartesHotel.alreadyPressed) {
			Defilement_Cartes ();
			cartesHotel.alreadyPressed = true;
		}
		if ((Mathf.Abs(Input.GetAxis (listeAxes.Defilement_Cartes)) <= 0.001f)  & (cartesHotel.alreadyPressed))
		{
			cartesHotel.alreadyPressed = false;
		}
	}

	// Quitter
	public void OnQuit ()
	{
		Application.Quit();
	}

	// Affiche ou masque les commandes
	public void Affiche_Masque_Commandes()
	{
		int userLayer = 8;
		isDisplayedCommands = !isDisplayedCommands;
		if (isDisplayedCommands) {
			// Desactive les boutons
			MenuCamera.gameObject.SetActive(true);
			Sortie.gameObject.SetActive(true);
			MenuTouches.gameObject.SetActive(true);

			MyCamera.cullingMask |= (1 << userLayer);
		}
		else {
			// Active les boutons
			MenuCamera.gameObject.SetActive(false);
			Sortie.gameObject.SetActive(false);
			MenuTouches.gameObject.SetActive(false);

			MyCamera.cullingMask &= ~(1 << userLayer);
		}
	}

	// Affiche les cartes
	public void Affiche_Cartes()
	{
		int Cartes_Layer = 9;
		cartesHotel.isDisplayedCartes = !cartesHotel.isDisplayedCartes;
		if (cartesHotel.isDisplayedCartes)
		{
			cartesHotel.hotelCourant = Plateau_Jeu.Hotel_Nom.Boomerang;
			MyCamera.cullingMask |= (1 << Cartes_Layer);
			Canvas_Cartes.GetComponentInChildren<Image> ().material = listeMaterialHotel [(int)cartesHotel.hotelCourant];
		}
		else
			MyCamera.cullingMask &= ~(1 << Cartes_Layer);
	}

	// Défilement des cartes
	public void Defilement_Cartes ()
	{
		if (cartesHotel.isDisplayedCartes)
		if (Input.GetAxis(listeAxes.Defilement_Cartes) > 0.0f)
			cartesHotel.hotelCourant = (Plateau_Jeu.Hotel_Nom)(((int)cartesHotel.hotelCourant + 1)%8);
		else
			cartesHotel.hotelCourant = (Plateau_Jeu.Hotel_Nom)(((int)cartesHotel.hotelCourant +7)%8);
		Canvas_Cartes.GetComponentInChildren<Image> ().material = listeMaterialHotel [(int)cartesHotel.hotelCourant];
	}

	// Mise à jour de la position du crosshair
	public void UpdateCrosshairControl()
	{
		Vector2 positionCrosshair = Crosshair.GetComponent<RectTransform> ().anchoredPosition;

		if (Mathf.Abs(positionCrosshair.x+Input.GetAxis (listeAxes.CrossHair_H) * Vitesse_Crosshair_H) < Screen.width/2.0)
			positionCrosshair.x += Input.GetAxis (listeAxes.CrossHair_H) * Vitesse_Crosshair_H;
		if (Mathf.Abs(positionCrosshair.y -Input.GetAxis (listeAxes.CrossHair_V) * Vitesse_Crosshair_V)< Screen.height/2.0)
		positionCrosshair.y -= Input.GetAxis (listeAxes.CrossHair_V) * Vitesse_Crosshair_V;

		Crosshair.GetComponent<RectTransform> ().anchoredPosition = positionCrosshair;

		if (Input.GetButtonDown (listeAxes.Bouton_Valid)) {
			Vector3 Point_Screen = new Vector3 (positionCrosshair.x + Screen.width / 2.0f, positionCrosshair.y + Screen.height / 2.0f, 1.0f);
			Vector3 Point_World = MyCamera.ScreenToWorldPoint (Point_Screen);
			Ray ray_commandes = new Ray (MyCamera.transform.position, Point_World - MyCamera.transform.position);

			RaycastHit hit_commandes,hit_plateau;
			if (Physics.Raycast (ray_commandes, out hit_commandes,700.0f,1<<8 + 1<<12)) {
				switch (hit_commandes.transform.name)
				{
				// Les mouvements camera
				case "CameraD":
					trigCommandes.CamD = true;
					break;
				case "CameraG":
					trigCommandes.CamG = true;
					break;
				case "CameraH":
					trigCommandes.CamH = true;
					break;
				case "CameraB":
					trigCommandes.CamB = true;
					break;
				case "CameraRapproche":
					trigCommandes.CamRapproche = true;
					break;
				case "CameraEloigne":
					trigCommandes.CamEloigne = true;
					break;
				case "CameraFocus":
					trigCommandes.CamFocus = true;
					break;

					// La boite de dialogue à OK
				case "Bouton1":
					choixDialogue = Choix_Dialogue.Oui;
					break;

					// La boite de dialogue à KO
				case "Bouton2":
					choixDialogue = Choix_Dialogue.Non;
					break;

					// Le lancé de dé
				case "BoutonLanderDé":
					trigCommandes.LanceDé = true;
					break;

					// Bouton lancer dé construction
				case "BoutonLancerDéConstruction":
					break;

					// Bouton Acheter
				case "Acheter":
					break;

					// Bouton Voir Proprio
				case "VoirProprio":
					trigCommandes.VoirProprio = true;
					break;

					// Bouton fin de tour
				case "FinTour":
					Etat_Partie.To_Fin_Tour_Joueur ();
					break;

					// Bouton quitter
				case "Quitter":
					trigCommandes.Quitter = true;
					break;

					// Bouton masquer/afficher les commandes
				case "Masquer_Commandes":
					trigCommandes.MasqueCommandes = true;
					break;
				}
			}
			if (Physics.Raycast (ray_commandes, out hit_plateau,7000.0f,1<<13)) {
				if (hit_plateau.rigidbody != null) {
					impactPlateau.touche = true;
					impactPlateau.PointPlateau = hit_plateau.point;
				} else {
					impactPlateau.touche = false;
					impactPlateau.PointPlateau = Vector3.zero;
				}
			}
		}
	}

	// Affichages des dés
	public void Affiche_Des()
	{
		int DeLayer = 10;
		int De_ConstructionLayer = 11;
		if (De.isVisible_De)
			MyCamera.cullingMask |= (1 << DeLayer);
		else
			MyCamera.cullingMask &= ~(1 << DeLayer);
		if (De.isVisible_De_Construction)
			MyCamera.cullingMask |= (1 << De_ConstructionLayer);
		else
			MyCamera.cullingMask &= ~(1 << De_ConstructionLayer);
	}

	// Gère la machine à état
	public void Gere_Machine_Etat ()
	{
		switch (Etat_Partie.etat) {
		// Début de partie
		case Etat_Partie.Etat.Debut_Partie:
			MachineEtatDebutPartie ();
			break;

		// Début du tour du joueur
		case Etat_Partie.Etat.Debut_Tour_Joueur:
			MachineEtatDebutTourJoueur ();
			break;

		// Lancer de dé 6
		case Etat_Partie.Etat.Lance_Dé:
			MachineEtatLancéDé ();
			break;

		// Mouvement de la voiture
		case Etat_Partie.Etat.MouvVoiture:
			MachineEtatMouvVoiture ();
			break;

		// Verif des 2000 euros
		case Etat_Partie.Etat.Verif_2000:
			MachineEtat2000euros ();
			break;

		// Nuit à l'hotel
		case Etat_Partie.Etat.Nuit_Hotel:
			MachineEtatNuitHotel ();
			break;

		// Analyse du type de case
		case Etat_Partie.Etat.Type_Case:
			MachineEtatTypeCase ();
			break;

		// Enchères
		case Etat_Partie.Etat.Encheres:
			MachineEtatEncheres ();
			break;

		// Achat entrées
		case Etat_Partie.Etat.Achat_Entrees:
			MachineEtatAchatEntrees ();
			break;

		// Fin du tour du joueur
		case Etat_Partie.Etat.Fin_Tour_Joueur:
			MachineEtatFinTourJoueur ();
			break;

		// Fin de la partie
		case Etat_Partie.Etat.Fin_Partie:
			MachineEtatFinPartie ();
			break;
		}
	}

	// Le dé roule ?
	public bool isRolling(GameObject un_dé)
	{
		if ((un_dé.GetComponent<Rigidbody> ().velocity == Vector3.zero) && (un_dé.GetComponent<Rigidbody> ().angularVelocity == Vector3.zero))
			return false;
		else
			return true;
	}

	// Determine quel type de saut
	private static Bezier.Type_Saut Type_Saut (int numcase)
	{
		Bezier.Type_Saut type_saut;
		switch (Plateau_Jeu.Jeu [numcase].etatOccupation) {
		case Plateau_Jeu.Etat_Occupation.Non_Occupe:
			switch (Plateau_Jeu.Jeu [(numcase + 1) % Plateau_Jeu.NB_CASES].etatOccupation) {
			case Plateau_Jeu.Etat_Occupation.Non_Occupe:
				type_saut = Bezier.Type_Saut.Bas_Bas;
				break;
			default:
				type_saut = Bezier.Type_Saut.Bas_Haut;
				break;
			}
			break;
		default:
			switch (Plateau_Jeu.Jeu [(numcase + 1) % Plateau_Jeu.NB_CASES].etatOccupation) {
			case Plateau_Jeu.Etat_Occupation.Non_Occupe:
				type_saut = Bezier.Type_Saut.Haut_Bas;
				break;
			default:
				type_saut = Bezier.Type_Saut.Haut_Haut;
				break;
			}
			break;

		}
		return type_saut;
	}

	// Getter sur la voiture courante
	private GameObject GetVoitureJoueurCourant()
	{
		return JeuScript.InfoVoitures.AllVoitures [(int)Joueur.joueurs [Joueur.GetJoueurActif ()].couleur];
	}

	// A t on fini d'avancer les cases pour les voitures ?
	private bool Fin_Cases_Avancées()
	{
		return (De.Case_courante_Joueur == De.Prochaine_Case);
	}

	// Machine etat debut de partie
	partial void MachineEtatDebutPartie();

	// Machine etat début de tour joueur
	partial void MachineEtatDebutTourJoueur();

	// Machine etat lancé de dé
 	partial void MachineEtatLancéDé();

	// Machine etat bouger la voiture
	partial void MachineEtatMouvVoiture();

	// Machine etat vérifications - Rajout 2000 euros
	partial void MachineEtat2000euros();

	// Machine etat Nuit a l'hotel
	partial void MachineEtatNuitHotel();

	// Machine etat encheres
	partial void MachineEtatEncheres();

	// Machine etat achat d'entrees
	partial void MachineEtatAchatEntrees();

	// Machine etat vérifications - Type de case
	partial void MachineEtatTypeCase();

	// Machine etat achat terrain
	partial void MachineEtatAchatTerrain();

	// Machine etat fin de tour du joueur
	partial void MachineEtatFinTourJoueur();

	// Machine etat fin de partie
	partial void MachineEtatFinPartie();

	// Reset du trigger des commandes crosshair
	void ResetTriggerCommandes()
	{
		trigCommandes.Acheter = false;
		trigCommandes.CamB = false;
		trigCommandes.CamD = false;
		trigCommandes.CamEloigne = false;
		trigCommandes.CamFocus = false;
		trigCommandes.CamG = false;
		trigCommandes.CamH = false;
		trigCommandes.CamRapproche = false;
		trigCommandes.FinTour = false;
		trigCommandes.LanceConstruction = false;
		trigCommandes.LanceDé = false;
		trigCommandes.Quitter = false;
		trigCommandes.MasqueCommandes = false;
		trigCommandes.VoirProprio = false;
	}

	// Avance une seule case
	public void Avance_Une_Case_Joueur()
	{
		if (Time.time - T0_Debut_Mouv_Voiture > Speed_Voiture) {
			Abscisse_Curv_T = 1.0f;
		} else {
			Abscisse_Curv_T = (Time.time - T0_Debut_Mouv_Voiture) / Speed_Voiture;
		}
		Vector3[] position_rotation_voiture = new Vector3[2]; 
		position_rotation_voiture = Bezier.CalculBezier (Abscisse_Curv_T, De.Case_courante_Joueur, JeuScript.Type_Saut (De.Case_courante_Joueur));
		GetVoitureJoueurCourant ().GetComponent<Transform> ().position = position_rotation_voiture[0];
		GetVoitureJoueurCourant ().GetComponent<Transform> ().rotation = Quaternion.Euler(position_rotation_voiture[1]);
	}

	// Fin de case
	public bool Fin_Une_Case()
	{
		return Abscisse_Curv_T == 1.0f;
	}

	public void AfficheDialogue(string Texte1,string Texte2,  bool isAfficheDialogue)
	{
		int DialogueLayer = 12;
		if (isAfficheDialogue) {
			Dialogue1.transform.parent.gameObject.SetActive (true);
			Dialogue1.text = Texte1;
			Dialogue2.text = Texte2;
			MyCamera.cullingMask |= (1 << DialogueLayer);
		} else {
			Dialogue1.transform.parent.gameObject.SetActive (false);
			MyCamera.cullingMask &= ~(1 << DialogueLayer);		
		}
	}

	// Mise à zero du choix de la boite de dialogue
	public void ResetChoixDialogue()
	{
		choixDialogue = Choix_Dialogue.Pas_Choisi;
	}

	// Choix à OK de la boite de dialogue
	public void SetOKChoixDialogue()
	{
		choixDialogue = Choix_Dialogue.Oui;
		AfficheDialogue ("", "", false);
	}

	// Choix à KO de la boite de dialogue
	public void SetKOChoixDialogue()
	{
		choixDialogue = Choix_Dialogue.Non;
		AfficheDialogue ("", "", false);
	}

	// Mise à jour du panneau d'infos du joueur
	public void SetPanelInfoJoueur(string texte)
	{
		texteJoueur.text = texte;
	}

	// Clear du panneau d'infos du joueur
	public void ClearPanelInfoJoueur()
	{
		texteJoueur.text = "";
	}

	// Recherche la case où l'on a cliqué, et est-ce possible de poser l'entrée ?
	public Plateau_Jeu.StructRechercheCaseEntree RechercheCaseEntree()
	{
		Plateau_Jeu.StructRechercheCaseEntree StructureRetour;
		bool retour = true;
		bool isProprietaireHotel = true;
		bool isEntreeDejaPresente = false;
		bool isArgentJoueur = true;

		float prix_entree;
		float min_distance = Mathf.Infinity;
		int case_min = 0;
		float distance_courante;
		Plateau_Jeu.Entree entree;

		// Recherche de la plus proche case voisine
		foreach (Plateau_Jeu.Case caseJeu in Plateau_Jeu.Jeu) {
			if (Vector3.Distance(new Vector3(caseJeu.Centre_Case.x, 0.0f, caseJeu.Centre_Case.y),impactPlateau.PointPlateau)<min_distance)
				{
					distance_courante = Vector3.Distance (new Vector3(caseJeu.Centre_Case.x, 0.0f, caseJeu.Centre_Case.y), impactPlateau.PointPlateau);
					min_distance = distance_courante;
					case_min = System.Array.IndexOf (Plateau_Jeu.Jeu, caseJeu);
				}
		}

		// De quel coté s'agit t'il ?
		if (Vector3.Cross (new Vector3 (Mathf.Cos (Mathf.Deg2Rad * (90.0f - Plateau_Jeu.Jeu [case_min].angle_case)),
			0.0f, Mathf.Sin (Mathf.Deg2Rad * (90.0f - Plateau_Jeu.Jeu [case_min].angle_case))),
			(impactPlateau.PointPlateau - new Vector3 (Plateau_Jeu.Jeu [case_min].Centre_Case.x,
				0.0f, Plateau_Jeu.Jeu [case_min].Centre_Case.y))).z > 0.0f) {
			// On est à droite de la case
			entree = System.Array.Find(Plateau_Jeu.Jeu[case_min].entree,w => w.cote == Plateau_Jeu.Cote.Droite);

		}
		else
		{
			// On est à gauche de la case
			entree = System.Array.Find(Plateau_Jeu.Jeu[case_min].entree,w => w.cote == Plateau_Jeu.Cote.Gauche);
		};

		// Déjà une entrée dessus, ou une entrée du coté opposé ?
		foreach (Plateau_Jeu.Entree ent in Plateau_Jeu.Jeu[case_min].entree)
			isEntreeDejaPresente = ent.hasProprio;

		// Propriétaire de cet hotel ?
		isProprietaireHotel = (entree.Proprietaire == Joueur.GetCouleurJoueurActif());

		// Récupération du prix de l'entrée
		prix_entree = Plateau_Jeu.ListeHotels.Find(x => x.Hotel_nom == entree.nomHotel).Entree;

		// Assez d'argent ?
		isArgentJoueur = (Joueur.joueurs[Joueur.GetJoueurActif()].argent >= prix_entree);
			
		// Peut-on construire une entrée ?
		retour = (!isEntreeDejaPresente & isProprietaireHotel & isArgentJoueur);
			
		StructureRetour.isPossible = retour;
		StructureRetour.numCaseEntree = case_min;
		StructureRetour.prixEntree = prix_entree;
		StructureRetour.entree = entree;

		return StructureRetour;
	}
}