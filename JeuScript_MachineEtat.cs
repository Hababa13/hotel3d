﻿using UnityEngine;

partial class JeuScript
{
// Machine etat debut de partie
	partial void MachineEtatDebutPartie()
	{
	if (Etat_Partie.transitionState.isTransitionDebutPartie) {
		SetPanelInfoJoueur("DEBUT DE PARTIE !!!");
		Etat_Partie.SetZeroTransition ();
	}
	if (Time.time - T0_Debut_partie > C_Tempo_debut_partie) {
		ClearPanelInfoJoueur();
		UpdatePanelJoueurs ();
		T0_Debut_Tour_Joueur = Time.time;
		Etat_Partie.To_Debut_Tour_Joueur ();
	}
	}

	// Machine etat début de tour joueur
	partial void MachineEtatDebutTourJoueur()
	{
		if (Etat_Partie.transitionState.isTransitionDebutTourJoueur) {
			SetPanelInfoJoueur("Joueur " + Joueur.GetStringCouleurJoueurActif () + " : Lancez les dés");
			Etat_Partie.SetZeroTransition ();
		}
		if (Time.time - T0_Debut_Tour_Joueur > C_Tempo_debut_joueur) {
			Etat_Partie.To_Lance_De ();
		}
	}

	// Machine etat lancé de dé
	partial void MachineEtatLancéDé()
	{
		if ((Input.GetAxis (listeAxes.Lance_De6) > 0.2f | trigCommandes.LanceDé) & !De.Dé6_Lancé) {

			De.Dé6_Lancé = true;
			trigCommandes.LanceDé = false;

			// Recalage des dés
			Dé6.GetComponent<Transform> ().position = De.De_position;
			Dé6.GetComponent<Transform> ().rotation = Quaternion.identity;

			De_Construction.GetComponent<Transform> ().position = De.De_Construction_Position;
			De_Construction.GetComponent<Transform> ().rotation = Quaternion.identity;

			// Affiche le dé 6 - cache le dé construction
			De.isVisible_De = true;
			De.isVisible_De_Construction = false;
			Affiche_Des ();

			// Genère un lancer aléatoire vertical
			Vector3 Force_aleatoire = new Vector3 (Force_De*Random.Range (-10.0f, 10.0f),
				Force_De*Random.Range (40.0f, 60.0f),
				Force_De*Random.Range (10.0f, 20.0f));
			Dé6.GetComponent<Rigidbody> ().AddForce (Force_aleatoire, ForceMode.Impulse);	
			Dé6.GetComponent<Rigidbody> ().angularVelocity = Random.onUnitSphere * Random.Range (6 * 6, 8 * 6);	
		}
		if (!isRolling(Dé6) & De.Dé6_Lancé) {
			De.Dé6_Lancé = false;
			float echelle_de = 1.0f;
			if (Vector3.Distance (Dé6.transform.TransformVector (Vector3.up), echelle_de*Vector3.up) < 0.01f) {
				De.De = 5;
			}
			if (Vector3.Distance (Dé6.transform.TransformVector (Vector3.down), echelle_de*Vector3.up) < 0.01f) {
				De.De = 2;
			}
			if (Vector3.Distance (Dé6.transform.TransformVector (Vector3.forward), echelle_de*Vector3.up) < 0.01f) {
				De.De = 1;
			}
			if (Vector3.Distance (Dé6.transform.TransformVector (Vector3.back), echelle_de*Vector3.up) < 0.01f) {
				De.De = 6;
			}
			if (Vector3.Distance (Dé6.transform.TransformVector (Vector3.left), echelle_de*Vector3.up) < 0.01f) {
				De.De = 3;
			}
			if (Vector3.Distance (Dé6.transform.TransformVector (Vector3.right), echelle_de*Vector3.up) < 0.01f) {
				De.De = 4;
			}

			// Mise à jour du champ De.Prochaine_Case
			De.Case_courante_Joueur = Joueur.joueurs[Joueur.GetJoueurActif()].case_Courante;
			De.Prochaine_Case = (De.De+De.Prochaine_Case)%Plateau_Jeu.NB_CASES;
			Joueur.joueurs[Joueur.GetJoueurActif()].case_Courante = (Joueur.joueurs[Joueur.GetJoueurActif()].case_Courante + De.De)%Plateau_Jeu.NB_CASES;
			while (Plateau_Jeu.isOccupee(De.Prochaine_Case))
			{
					De.Prochaine_Case++;
			}
					
			Etat_Partie.SetZeroTransition ();
			Etat_Partie.To_Mouv_Voiture ();
		}
	}

	// Machine etat bouger la voiture
	partial void MachineEtatMouvVoiture()
	{
		if (Etat_Partie.transitionState.isTransitionMouvVoiture) {
			T0_Debut_Mouv_Voiture = Time.time;
			Abscisse_Curv_T = 0.0f;
			Etat_Partie.SetZeroTransition ();
		} else {
			if (!Fin_Cases_Avancées ()) {
				if (!Fin_Une_Case()){
					Avance_Une_Case_Joueur();
				}
				else{
					De.Case_courante_Joueur = (De.Case_courante_Joueur + 1) % Plateau_Jeu.NB_CASES;
					Abscisse_Curv_T = 0.0f;
					T0_Debut_Mouv_Voiture = Time.time;
					Joueur.joueurs [Joueur.GetJoueurActif ()].premiercoup = false;
				}
			} else {
				Etat_Partie.To_Verif2000euros ();
			}
		}
	}

	// Machine etat vérifications - 2000 euros
	partial void MachineEtat2000euros()
	{
		// Verif des 2000 euros
		if ((De.Case_courante_Joueur > Case_2000) & (De.Case_courante_Joueur - De.De <= Case_2000)& Etat_Partie.transitionState.isTransition2000euros)
		{
			// OK on crédite 2000
			Joueur.joueurs[Joueur.GetJoueurActif()].argent += 2000.0f;
			Etat_Partie.SetZeroTransition ();
			T0_Debut_2000_euros = Time.time;
			is2000euros = true;
		}
		else
		{
			if (is2000euros) 
			{
				if (Time.time - T0_Debut_2000_euros > C_Tempo_2000_euros)
				{
					Etat_Partie.SetZeroTransition();
					Etat_Partie.To_NuitHotel();
				}
			} else
			{
				is2000euros = false;
				Etat_Partie.SetZeroTransition();
				Etat_Partie.To_NuitHotel();
			}
		}
	}

	// Machine etat nuit hotel
	partial void MachineEtatNuitHotel()
	{
		/// ERREUR !!!!!!!
		/// ERREUR !!!!!!!
		/// ERREUR !!!!!!!
		Joueur.Couleur nomProprio = Joueur.Couleur.Rouge; // Couleur par défaut

		// Chercher un propriétaire 
		if (Etat_Partie.transitionState.isTransitionNuitHotel){
			Etat_Partie.Reset_Ss_Etat_Nuit_Hotel();
			isPayerProprio = false;
			Etat_Partie.SetZeroTransition();
			foreach (Plateau_Jeu.Entree entree in Plateau_Jeu.Jeu[De.Case_courante_Joueur].entree)
			{
				if (entree.hasProprio & (entree.Proprietaire != Joueur.GetCouleurJoueurActif()))
				{
					nomProprio = entree.Proprietaire;
					nomHotelPayer = entree.nomHotel;
					isPayerProprio = true;
					isJoueurDoitPayerProprio = true;
					break;
				}
			}
		}
		else {
			if (isPayerProprio & isJoueurDoitPayerProprio)
			{
				// Payer le proprio : Lancer le dé
				JeuScript.De.Nombre_Nuits = Mathf.FloorToInt(Random.Range(0.0f, 6.0f)); 
			 	
				// Recherche du tarif
				float somme_a_payer = Plateau_Jeu.ListeHotels.Find(x=> x.Hotel_nom == nomHotelPayer).Prix_Nuit[Plateau_Jeu.ListeHotels.Find(x=> x.Hotel_nom == nomHotelPayer).Nb_Construction]*JeuScript.De.Nombre_Nuits;

				// Paiement
				bool paiement_effectué = Joueur.SetPaiementJoueur(Joueur.GetCouleurJoueurActif(),somme_a_payer);
				Joueur.SetCreditJoueur(nomProprio,somme_a_payer);

				if (!paiement_effectué)
				{
					Etat_Partie.To_Encheres();
				}

				// Le joueur a payé ses nuits
				isJoueurDoitPayerProprio = false;
			}
			else
			{
				// Le joueur a payé le proprio, ou bien pas de proprio
				Etat_Partie.To_AchatEntrees();
			}
		}
	}

	// Machine etat encheres
	partial void MachineEtatEncheres()
	{
		
	}
		
	// Machine etat achat d'entrees
	partial void MachineEtatAchatEntrees()
	{
		if (Etat_Partie.transitionState.isTransitionAchatEntrees)
		{
			Etat_Partie.Reset_Ss_Etat_Achat_Entree();
			// Verif des achats d'entrées
			if (((De.Case_courante_Joueur - Case_Achat_Entrees)%Plateau_Jeu.NB_CASES <= 6) & (De.Case_courante_Joueur - De.De <= Case_Achat_Entrees) & (Etat_Partie.transitionState.isTransitionAchatEntrees))
			{
				// Demande si achat entrées
				isPossibleEntree = true;
				// MISE A JOUR DE LA BOITE DE DIALOGUE
				AfficheDialogue("ACHAT ENTREE", "PAS D'ACHAT ENTREE", true);
				Etat_Partie.transitionState.isTransitionAchatEntrees = false;
				Etat_Partie.ss_Etat_Achat_Entree = Etat_Partie.Ss_Etat_Achat_Entree.Non_Applicable;
				Etat_Partie.To_AchatEntree_ChoixHotel();

			}
		} else
		{
			// Si pas d'achat possible ou bien on choisit de ne pas acheter d'entrée
			if ((!isPossibleEntree) | (JeuScript.choixDialogue == Choix_Dialogue.Non))
				Etat_Partie.ss_Etat_Achat_Entree = Etat_Partie.Ss_Etat_Achat_Entree.Fin_Ss_Etat;
			else
			{
				if (JeuScript.choixDialogue != Choix_Dialogue.Oui)
				{
					switch (Etat_Partie.ss_Etat_Achat_Entree)
					{
						// Choix hotel
						case Etat_Partie.Ss_Etat_Achat_Entree.Choix_Hotel:
							if (Etat_Partie.transitionStateAchatEntree.isTransitionChoixHotel)
							{
								// Affiche le panel et dit qu'il faut choisir une entrée
								SetPanelInfoJoueur("Choisissez une entrée...");
								Etat_Partie.transitionStateAchatEntree.isTransitionChoixHotel = false;
							}
							// Si on a sélectionné une entrée
							if (impactPlateau.touche)
							{
								Plateau_Jeu.Analyse_Case_Entree = RechercheCaseEntree();
							if (!Plateau_Jeu.Analyse_Case_Entree.isPossible)
								{	
									//Pas possible de mettre une entrée : pas sur une case plateau, entrée déja occupée, autre coté déja posé, pas proprio, pas assez d'argent
									Etat_Partie.To_AchatEntrees();
								} else Etat_Partie.To_AchatEntree_Paiement();
							}
							else Etat_Partie.To_AchatEntree_Paiement();
						break;
				
						// Paiement
						case Etat_Partie.Ss_Etat_Achat_Entree.Paiement:
							Joueur.joueurs[Joueur.GetJoueurActif()].argent -= Plateau_Jeu.Analyse_Case_Entree.prixEntree;
							Etat_Partie.To_AchatEntree_PlacementEntree();
						break;

						// Placement de l'entree dans l'hotel
						case Etat_Partie.Ss_Etat_Achat_Entree.Placer_Entree:
							Vector3 Centre_Case3 = new Vector3(Plateau_Jeu.Jeu[Plateau_Jeu.Analyse_Case_Entree.numCaseEntree].Centre_Case.x, 0.0f, Plateau_Jeu.Jeu[Plateau_Jeu.Analyse_Case_Entree.numCaseEntree].Centre_Case.y);
							Vector3 pos_entree = Centre_Case3 + (Plateau_Jeu.Analyse_Case_Entree.entree.cote==Plateau_Jeu.Cote.Droite?1.0f:-1.0f)*Plateau_Jeu.largeurCase*
								Vector3.Cross(Vector3.up,
								new Vector3(Mathf.Cos(Mathf.Deg2Rad* (90.0f - Plateau_Jeu.Jeu[Plateau_Jeu.Analyse_Case_Entree.numCaseEntree].angle_case)),
								0.0f,
								Mathf.Sin(Mathf.Deg2Rad* (90.0f - Plateau_Jeu.Jeu[Plateau_Jeu.Analyse_Case_Entree.numCaseEntree].angle_case))));
						
							Quaternion quater_entree = new Quaternion();

							quater_entree.eulerAngles = new Vector3(0.0f,
							Plateau_Jeu.Jeu[Plateau_Jeu.Analyse_Case_Entree.numCaseEntree].angle_case+(Plateau_Jeu.Analyse_Case_Entree.entree.cote == Plateau_Jeu.Cote.Droite?90.0f:-90.0f),0.0f);
							
							Instantiate(UneEntree,pos_entree,quater_entree);
							Etat_Partie.To_AchatEntrees();
						break;

						// Sortie du sous etat
						case Etat_Partie.Ss_Etat_Achat_Entree.Fin_Ss_Etat:
							Etat_Partie.SetZeroTransition();
							Etat_Partie.To_Type_Case();
							break;
					}
				}
			}
		}
	}

	// Machine etat vérifications - Type de case
	partial void MachineEtatTypeCase()
	{

		// Quel est le type de la case courante ?
		switch (Plateau_Jeu.Jeu[De.Case_courante_Joueur].typeCase)
		{
		case Plateau_Jeu.TypeCase.Achat:
			Etat_Partie.To_AchatTerrain();
			break;
		case Plateau_Jeu.TypeCase.Construction:
			Etat_Partie.To_Construction();
			break;
		case Plateau_Jeu.TypeCase.Construction_Gratuite:
			Etat_Partie.To_ConstructionGratuite();
			break;
		case Plateau_Jeu.TypeCase.Entrees:
			Etat_Partie.To_AchatEntrees();
			break;
		}
	}


	// Machine etat achat terrain
	partial void MachineEtatAchatTerrain()
	{
		if (Etat_Partie.transitionState.isTransitionAchatTerrain)
		{
			Etat_Partie.Reset_Ss_Etat_Achat_Terrain();
			bool isHotelAchetable = false;
			// Verif des achats d'entrées
			foreach(Plateau_Jeu.Hotel hotel in Plateau_Jeu.Jeu[De.Case_courante_Joueur].hotel)
				if (hotel.Proprietaire)
					isHotelAchetable = true;

			if (isHotelAchetable)
			{
				// MISE A JOUR DE LA BOITE DE DIALOGUE
				AfficheDialogue("ACHAT TERRAIN", "PAS D'ACHAT TERRAIN", true);
				Etat_Partie.transitionState.isTransitionAchatTerrain = false;
				Etat_Partie.ss_Etat_Achat_Terrain = Etat_Partie.Ss_Etat_Achat_Terrain.Non_Applicable;
				Etat_Partie.To_AchatTerrain_ChoixTerrain();
			}
		} else
		{
			switch (Etat_Partie.ss_Etat_Achat_Terrain)
			{
			case Etat_Partie.Ss_Etat_Achat_Terrain.Choix_Terrain:
				break;
			case Etat_Partie.Ss_Etat_Achat_Terrain.Paiement:
				break;
			case Etat_Partie.Ss_Etat_Achat_Terrain.Fin_Ss_Etat:
				break;
			}

		}
				
	}

	// Machine etat fin de tour du joueur
	partial void MachineEtatFinTourJoueur()
	{
		if (Joueur.nb_joueurs == 1)
			Etat_Partie.To_Fin_Partie ();
		else {
			Joueur.SetProchain_Joueur ();
			Etat_Partie.To_Debut_Tour_Joueur ();
		}
	}

	// Machine etat fin de partie
	partial void MachineEtatFinPartie()
	{
		if (Etat_Partie.transitionState.isTransitionFinPartie) {
			T0_Fin_Partie = Time.time;
			SetPanelInfoJoueur("FIN DE PARTIE, " + Joueur.GetStringCouleurJoueurActif () + "GAGNE !");
		} else
			if (Time.time - T0_Fin_Partie > C_Tempo_fin_partie)
				OnQuit();
	}

}