﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plateau_Jeu : MonoBehaviour {
	public GameObject B1;

	public enum TypeCase  {Achat, Construction, Entrees, Construction_Gratuite}

	public enum Hotel_Nom {Boomerang, Fujiyama, Etoile, President, Royal, Waikiki, TajMahal, Safari}

	public enum Cote {Gauche, Droite}

	public enum Etat_Occupation {Non_Occupe, Rouge, Vert, Bleu, Jaune}

	public struct Entree
	{
		public bool hasProprio;
		public Hotel_Nom nomHotel;
		public Joueur.Couleur Proprietaire;
		public Cote cote;
	}

	public struct Batiment {
		public Vector2 emplacement;
		public bool isConstruit;
		public GameObject Bat3D;
	}

	public struct Hotel
	{ 
		public Hotel_Nom Hotel_nom;
		public float Terrain;
		public float Achat_obligatoire;
		public float Entree;
		public float Batiment_principal;
		public float[] Annexes;
		public int Nb_Annexes;
		public float Base_Loisirs;
		public float[] Prix_Nuit;
		public int Nb_Construction;
		public bool Proprietaire;
		public Joueur.Couleur CouleurProprietaire;
		public Batiment[] batiments;
	};

	public struct Case 
	{
		public int numCase;
		public TypeCase typeCase;
		public Vector2 Centre_Case;
		public Hotel[] hotel;
		public Entree[] entree;
		public float angle_case;
		public Etat_Occupation etatOccupation;
	}

	public const float largeurCase = 15.0f;
	public const int NB_CASES = 31;

	public static Case[] Jeu = new Case[NB_CASES];
	public static Hotel h_Boomerang;
	public static Hotel h_Fujiyama;
	public static Hotel h_Etoile;
	public static Hotel h_President;
	public static Hotel h_Royal;
	public static Hotel h_Waikiki;
	public static Hotel h_TajMahal;
	public static Hotel h_Safari;

	public static Batiment Mairie;
	public static Batiment Banque;

	public static List<Hotel> ListeHotels = new List<Hotel>();

	public struct StructRechercheCaseEntree
	{
		public bool isPossible;
		public float prixEntree;
		public int numCaseEntree;
		public Entree entree;
	}

	public static StructRechercheCaseEntree Analyse_Case_Entree;

	// Use this for initialization
	void Start () {
		// Initialisation des batiments
		Batiment[] batiments_Boomerang = new Batiment[2];
		Batiment[] batiments_Fujiyama = new Batiment[4];
		Batiment[] batiments_Etoile = new Batiment[6];
		Batiment[] batiments_President = new Batiment[5];
		Batiment[] batiments_Royal = new Batiment[5];
		Batiment[] batiments_Waikiki = new Batiment[6];
		Batiment[] batiments_TajMahal = new Batiment[4];
		Batiment[] batiments_Safari = new Batiment[4];

		// Batiment Mairie et Banque
		SetBatiment (Mairie, new Vector2 (-33.0f, -42.0f), true, B1);
		SetBatiment (Banque, new Vector2 (-21.4f, -63.3f), true, B1);

		// Batiments boomerang
		SetBatiment (batiments_Boomerang[0], new Vector2 (-77.0f, 63.0f), false, B1);
		SetBatiment (batiments_Boomerang[1], new Vector2 (-72.0f, 54.0f), false, B1);

		// Batiments Fujiyama
		SetBatiment (batiments_Fujiyama[0], new Vector2 (-50.0f, 20.0f), false, B1);
		SetBatiment (batiments_Fujiyama[1], new Vector2 (-59.0f, 13.0f), false, B1);
		SetBatiment (batiments_Fujiyama[2], new Vector2 (-39.0f, 15.0f), false, B1);
		SetBatiment (batiments_Fujiyama[3], new Vector2 (-50.0f, 30.0f), false, B1);

		// Batiments Etoile
		SetBatiment (batiments_Etoile[0], new Vector2 (4.0f, 25.0f), false, B1);
		SetBatiment (batiments_Etoile[1], new Vector2 (-14.0f, 19.0f), false, B1);
		SetBatiment (batiments_Etoile[2], new Vector2 (17.0f, 10.0f), false, B1);
		SetBatiment (batiments_Etoile[3], new Vector2 (-15.0f, 2.0f), false, B1);
		SetBatiment (batiments_Etoile[4], new Vector2 (10.0f, -5.0f), false, B1);
		SetBatiment (batiments_Etoile[5], new Vector2 (0.0f, 10.0f), false, B1);

		// Batiments President
		SetBatiment (batiments_President[0], new Vector2 (82.0f, 60.0f), false, B1);
		SetBatiment (batiments_President[1], new Vector2 (82.0f, 46.0f), false, B1);
		SetBatiment (batiments_President[2], new Vector2 (68.0f, 60.0f), false, B1);
		SetBatiment (batiments_President[3], new Vector2 (54.5f, 60.0f), false, B1);
		SetBatiment (batiments_President[4], new Vector2 (67.0f, 48.5f), false, B1);

		// Batiments Royal
		SetBatiment (batiments_Royal[0], new Vector2 (45.0f, -6.0f), false, B1);
		SetBatiment (batiments_Royal[1], new Vector2 (56.0f, -12.0f), false, B1);
		SetBatiment (batiments_Royal[2], new Vector2 (31.0f, -4.0f), false, B1);
		SetBatiment (batiments_Royal[3], new Vector2 (42.0f, -15.0f), false, B1);
		SetBatiment (batiments_Royal[4], new Vector2 (48.0f, 4.0f), false, B1);

		// Batiments Waikiki
		SetBatiment (batiments_Waikiki[0], new Vector2 (43.0f, -60.0f), false, B1);
		SetBatiment (batiments_Waikiki[1], new Vector2 (58.0f, -65.0f), false, B1);
		SetBatiment (batiments_Waikiki[2], new Vector2 (31.0f, -4.0f), false, B1);
		SetBatiment (batiments_Waikiki[3], new Vector2 (69.0f, -60.0f), false, B1);
		SetBatiment (batiments_Waikiki[4], new Vector2 (75.0f, -51.5f), false, B1);
		SetBatiment (batiments_Waikiki[5], new Vector2 (57.0f, -52.0f), false, B1);

		// Batiments TajMahal
		SetBatiment (batiments_TajMahal[0], new Vector2 (9.5f, -64f), false, B1);
		SetBatiment (batiments_TajMahal[1], new Vector2 (19.0f, -58.0f), false, B1);
		SetBatiment (batiments_TajMahal[2], new Vector2 (0.0f, -58.0f), false, B1);
		SetBatiment (batiments_TajMahal[3], new Vector2 (10.0f, -50.0f), false, B1);

		// Batiments Safari
		SetBatiment (batiments_Safari[0], new Vector2 (-75.0f, -65.0f), false, B1);
		SetBatiment (batiments_Safari[1], new Vector2 (-84.0f, -51.0f), false, B1);
		SetBatiment (batiments_Safari[2], new Vector2 (-85.0f, -36.0f), false, B1);
		SetBatiment (batiments_Safari[3], new Vector2 (-74.0f, -51.0f), false, B1);

		// Initialisation des hotels
		Plateau_Jeu.SetHotel (Plateau_Jeu.h_Boomerang, Plateau_Jeu.Hotel_Nom.Boomerang, 500.0f, 250.0f, 100.0f, 1800.0f, null, 0, 250.0f, new float[2] {400.0f, 600.0f}, 0, false, Joueur.Couleur.Rouge, batiments_Boomerang);
		Plateau_Jeu.SetHotel (Plateau_Jeu.h_Fujiyama, Plateau_Jeu.Hotel_Nom.Fujiyama, 1000.0f, 500.0f, 100.0f, 2200.0f, new float[2] {1400.0f, 1400.0f}, 2, 500.0f, new float[4] {50.0f, 50.0f, 100.0f, 250.0f}, 0, false, Joueur.Couleur.Rouge, batiments_Fujiyama);
		Plateau_Jeu.SetHotel (Plateau_Jeu.h_Etoile, Plateau_Jeu.Hotel_Nom.Etoile, 3000.0f, 1500.0f, 250.0f, 3300.0f, new float[4] {2200.0f, 1800.0f, 1800.0f, 1800.0f}, 4, 500.0f, new float[6] {150.0f, 300.0f, 300.0f, 300.0f, 450.0f, 750.0f}, 0, false, Joueur.Couleur.Rouge, batiments_Etoile);
		Plateau_Jeu.SetHotel (Plateau_Jeu.h_President, Plateau_Jeu.Hotel_Nom.President, 3500.0f, 1750.0f, 250.0f, 5000.0f, new float[3] {3000.0f, 2250.0f, 1750.0f}, 3, 5000.0f, new float[5] {200.0f, 400.0f, 600.0f, 800.0f, 1200.0f}, 0, false, Joueur.Couleur.Rouge, batiments_President);
		Plateau_Jeu.SetHotel (Plateau_Jeu.h_Royal, Plateau_Jeu.Hotel_Nom.Royal, 2500.0f, 1250.0f, 200.0f, 3600.0f, new float[3] {2600.0f, 1800.0f, 1800.0f}, 3, 3000.0f, new float[5] {150.0f, 300.0f, 300.0f, 4500.0f, 600.0f}, 0, false, Joueur.Couleur.Rouge, batiments_Royal);
		Plateau_Jeu.SetHotel (Plateau_Jeu.h_Waikiki, Plateau_Jeu.Hotel_Nom.Waikiki, 2500.0f, 1250.0f, 200.0f, 3500.0f, new float[4] {2500.0f, 2500.0f, 1750.0f, 1750.0f}, 4, 2500.0f, new float[6] {200.0f, 350.0f, 500.0f, 500.0f, 650.0f, 1000.0f}, 0, false, Joueur.Couleur.Rouge, batiments_Waikiki);
		Plateau_Jeu.SetHotel (Plateau_Jeu.h_TajMahal, Plateau_Jeu.Hotel_Nom.TajMahal, 1500.0f, 750.0f, 100.0f, 2400.0f, new float[2] {1000.0f, 500.0f}, 2, 1000.0f, new float[4] {100.0f, 100.0f, 200.0f, 300.0f}, 0, false, Joueur.Couleur.Rouge, batiments_TajMahal);
		Plateau_Jeu.SetHotel (Plateau_Jeu.h_Safari, Plateau_Jeu.Hotel_Nom.Safari, 2000.0f, 1000.0f, 150.0f, 2600.0f, new float[2] {1200.0f, 1200.0f}, 2, 2000.0f, new float[4] {100.0f, 100.0f, 250.0f, 500.0f}, 0, false, Joueur.Couleur.Rouge, batiments_Safari);
	
		ListeHotels.Add (h_Boomerang);
		ListeHotels.Add (h_Fujiyama);
		ListeHotels.Add (h_Etoile);
		ListeHotels.Add (h_President);
		ListeHotels.Add (h_Royal);
		ListeHotels.Add (h_Waikiki);
		ListeHotels.Add (h_TajMahal);
		ListeHotels.Add (h_Safari);

		Analyse_Case_Entree = Plateau_Jeu.ResetAnalyseCaseEntree();

		// Initialisation des cases
		Plateau_Jeu.SetCase (0, TypeCase.Construction_Gratuite, new Vector2 (-78.0f, -6.0f),null, 302.0f,null, Etat_Occupation.Non_Occupe);

		Entree[] entree1 = new Entree[1] {SetEntree(false, Hotel_Nom.Boomerang, Joueur.Couleur.Rouge, Cote.Droite)};
		Plateau_Jeu.SetCase (1, TypeCase.Construction, new Vector2 (-83.0f, 12.0f),new Hotel[1] {h_Fujiyama}, 0.0f,entree1, Etat_Occupation.Non_Occupe);

		Entree entree2_g = SetEntree(false, Hotel_Nom.Boomerang, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree2_d = SetEntree(false, Hotel_Nom.Fujiyama, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree2 = new Entree[2] {entree2_g, entree2_d};
		Plateau_Jeu.SetCase (2, TypeCase.Achat, new Vector2 (-81.0f, 28.0f),new Hotel[2] {h_Boomerang, h_Fujiyama},21.13f,entree2, Etat_Occupation.Non_Occupe);

		Entree entree3_g = SetEntree(false, Hotel_Nom.Boomerang, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree3_d = SetEntree(false, Hotel_Nom.Fujiyama, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree3 = new Entree[2] {entree3_g, entree3_d};
		Plateau_Jeu.SetCase (3, TypeCase.Construction, new Vector2 (-72.0f, 39.0f),new Hotel[2] {h_Boomerang, h_Fujiyama}, 46.13f,entree3, Etat_Occupation.Non_Occupe);

		Entree entree4_g = SetEntree(false, Hotel_Nom.Boomerang, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree4_d = SetEntree(false, Hotel_Nom.Fujiyama, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree4 = new Entree[2] {entree4_g, entree4_d};
		Plateau_Jeu.SetCase (4, TypeCase.Achat, new Vector2 (-59f, 46.0f),new Hotel[2] {h_Boomerang, h_Fujiyama},70.6f,entree4, Etat_Occupation.Non_Occupe);

		Entree entree5_g = SetEntree(false, Hotel_Nom.Boomerang, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree5_d = SetEntree(false, Hotel_Nom.Fujiyama, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree5 = new Entree[2] {entree5_g, entree5_d};
		Plateau_Jeu.SetCase (5, TypeCase.Construction, new Vector2 (-45.0f, 46.0f),new Hotel[2] {h_Boomerang, h_Fujiyama}, 103.8f,entree5, Etat_Occupation.Non_Occupe);

		Entree[] entree6 = new Entree[1] { SetEntree (false, Hotel_Nom.Fujiyama, Joueur.Couleur.Rouge, Cote.Droite) };
		Plateau_Jeu.SetCase (6, TypeCase.Entrees, new Vector2 (-32.0f, 41.0f),new Hotel[1] {h_Fujiyama},113.8f,entree6, Etat_Occupation.Non_Occupe);

		// -----------------------------------------------------------------------------------------
		Plateau_Jeu.SetCase (7, TypeCase.Construction, new Vector2 (-16.5f, 39.5f),null, 73.7f,null, Etat_Occupation.Non_Occupe);

		Entree[] entree8 = new Entree[1] { SetEntree (false, Hotel_Nom.Etoile, Joueur.Couleur.Rouge, Cote.Droite) };
		Plateau_Jeu.SetCase (8, TypeCase.Achat, new Vector2 (-3.0f, 45.0f),new Hotel[1] {h_Etoile},73.7f,entree8, Etat_Occupation.Non_Occupe);

		Entree entree9_g = SetEntree(false, Hotel_Nom.President, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree9_d = SetEntree(false, Hotel_Nom.Etoile, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree9 = new Entree[2] {entree9_g, entree9_d};
		Plateau_Jeu.SetCase (9, TypeCase.Achat, new Vector2 (11.5f, 41.5f),new Hotel[2] {h_Etoile, h_President}, 129.4f,entree9, Etat_Occupation.Non_Occupe);

		Entree entree10_g = SetEntree(false, Hotel_Nom.President, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree10_d = SetEntree(false, Hotel_Nom.Etoile, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree10 = new Entree[2] {entree10_g, entree10_d};
		Plateau_Jeu.SetCase (10, TypeCase.Construction_Gratuite, new Vector2 (23.0f, 32.0f),new Hotel[2] {h_Etoile, h_President},132.3f,entree10, Etat_Occupation.Non_Occupe);

		Entree entree11_g = SetEntree(false, Hotel_Nom.President, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree11_d = SetEntree(false, Hotel_Nom.Royal, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree11 = new Entree[2] {entree11_g, entree11_d};
		Plateau_Jeu.SetCase (11, TypeCase.Achat, new Vector2 (35.0f, 23.0f),new Hotel[2] {h_President, h_Royal}, 114.0f,entree11, Etat_Occupation.Non_Occupe);

		Entree entree12_g = SetEntree(false, Hotel_Nom.President, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree12_d = SetEntree(false, Hotel_Nom.Royal, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree12 = new Entree[2] {entree12_g, entree12_d};
		Plateau_Jeu.SetCase (12, TypeCase.Construction, new Vector2 (50.0f, 19.0f),new Hotel[2] {h_President, h_Royal}, 97.7f,entree12, Etat_Occupation.Non_Occupe);

		Entree entree13_g = SetEntree(false, Hotel_Nom.President, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree13_d = SetEntree(false, Hotel_Nom.Royal, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree13 = new Entree[2] {entree13_g, entree13_d};
		Plateau_Jeu.SetCase (13, TypeCase.Achat, new Vector2 (65.0f, 17.0f),new Hotel[2] {h_President, h_Royal}, 105.0f,entree13, Etat_Occupation.Non_Occupe);

		Entree entree14_g = SetEntree(false, Hotel_Nom.President, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree14_d = SetEntree(false, Hotel_Nom.Royal, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree14 = new Entree[2] {entree14_g, entree14_d};
		Plateau_Jeu.SetCase (14, TypeCase.Construction, new Vector2 (77.0f, 9.0f),new Hotel[2] {h_President, h_Royal}, 140.0f,entree14, Etat_Occupation.Non_Occupe);

		Entree entree15_g = SetEntree(false, Hotel_Nom.President, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree15_d = SetEntree(false, Hotel_Nom.Royal, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree15 = new Entree[2] {entree15_g, entree15_d};
		Plateau_Jeu.SetCase (15, TypeCase.Achat, new Vector2 (83.0f, -3.0f),new Hotel[2] {h_President, h_Royal}, 175.0f,entree15, Etat_Occupation.Non_Occupe);

		// -----------------------------------------------------------------------------------------
		Entree entree16_g = SetEntree(false, Hotel_Nom.Waikiki, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree16_d = SetEntree(false, Hotel_Nom.Royal, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree16 = new Entree[2] {entree16_g, entree16_d};
		Plateau_Jeu.SetCase (16, TypeCase.Construction, new Vector2 (81.0f, -19.0f),new Hotel[2] {h_Waikiki, h_Royal}, 206.0f,entree16, Etat_Occupation.Non_Occupe);

		Entree entree17_g = SetEntree(false, Hotel_Nom.Waikiki, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree17_d = SetEntree(false, Hotel_Nom.Royal, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree17 = new Entree[2] {entree17_g, entree17_d};
		Plateau_Jeu.SetCase (17, TypeCase.Achat, new Vector2 (71.0f, -30.0f),new Hotel[2] {h_Waikiki, h_Royal}, 238.0f,entree17, Etat_Occupation.Non_Occupe);

		Entree entree18_g = SetEntree(false, Hotel_Nom.Waikiki, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree18_d = SetEntree(false, Hotel_Nom.Royal, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree18 = new Entree[2] {entree18_g, entree18_d};
		Plateau_Jeu.SetCase (18, TypeCase.Entrees, new Vector2 (58.0f, -34.0f),new Hotel[2] {h_Waikiki, h_Royal}, 272.0f,entree18, Etat_Occupation.Non_Occupe);

		Entree entree19_g = SetEntree(false, Hotel_Nom.Waikiki, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree19_d = SetEntree(false, Hotel_Nom.Royal, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree19 = new Entree[2] {entree19_g, entree19_d};
		Plateau_Jeu.SetCase (19, TypeCase.Construction, new Vector2 (42.0f, -31.0f),new Hotel[2] {h_Waikiki, h_Royal}, 287.0f,entree19, Etat_Occupation.Non_Occupe);

		Entree entree20_g = SetEntree(false, Hotel_Nom.Waikiki, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree20_d = SetEntree(false, Hotel_Nom.Royal, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree20 = new Entree[2] {entree20_g, entree20_d};
		Plateau_Jeu.SetCase (20, TypeCase.Achat, new Vector2 (29.0f, -27.0f),new Hotel[2] {h_Waikiki, h_Royal}, 287.0f,entree20, Etat_Occupation.Non_Occupe);

		// -----------------------------------------------------------------------------------------

		Entree entree21_g = SetEntree(false, Hotel_Nom.TajMahal, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree21_d = SetEntree(false, Hotel_Nom.Etoile, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree21 = new Entree[2] {entree21_g, entree21_d};
		Plateau_Jeu.SetCase (21, TypeCase.Achat, new Vector2 (14.0f, -23.0f),new Hotel[2] {h_TajMahal, h_Etoile},287.0f,entree21, Etat_Occupation.Non_Occupe);

		Entree entree22_g = SetEntree(false, Hotel_Nom.TajMahal, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree22_d = SetEntree(false, Hotel_Nom.Etoile, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree22 = new Entree[2] {entree22_g, entree22_d};
		Plateau_Jeu.SetCase (22, TypeCase.Construction, new Vector2 (-2.0f, -20.0f),new Hotel[2] {h_TajMahal, h_Etoile}, 254.5f,entree22, Etat_Occupation.Non_Occupe);

		Entree entree23_g = SetEntree(false, Hotel_Nom.TajMahal, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree23_d = SetEntree(false, Hotel_Nom.Etoile, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree23 = new Entree[2] {entree23_g, entree23_d};
		Plateau_Jeu.SetCase (23, TypeCase.Achat, new Vector2 (-13.0f, -30.0f),new Hotel[2] {h_TajMahal, h_Etoile}, 204.3f,entree23, Etat_Occupation.Non_Occupe);

		Entree entree24_g = SetEntree(false, Hotel_Nom.TajMahal, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree24_d = SetEntree(false, Hotel_Nom.Etoile, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree24 = new Entree[2] {entree24_g, entree24_d};
		Plateau_Jeu.SetCase (24, TypeCase.Construction_Gratuite, new Vector2 (-14.0f, -44.0f),new Hotel[2] {h_TajMahal, h_Etoile}, 0.0f,entree24, Etat_Occupation.Non_Occupe);

		Entree[] entree25 = new Entree[1] { SetEntree (false, Hotel_Nom.TajMahal, Joueur.Couleur.Rouge, Cote.Gauche) };
		Plateau_Jeu.SetCase (25, TypeCase.Construction, new Vector2 (-20.0f, -59.0f),new Hotel[2] {h_TajMahal, h_Etoile}, 228.0f,entree25, Etat_Occupation.Non_Occupe);

		// -----------------------------------------------------------------------------------------

		Entree[] entree26 = new Entree[1] { SetEntree (false, Hotel_Nom.Safari, Joueur.Couleur.Rouge, Cote.Gauche) };
		Plateau_Jeu.SetCase (26, TypeCase.Construction, new Vector2 (-36.0f, -63.0f),new Hotel[1] {h_Safari}, 287.0f,entree26, Etat_Occupation.Non_Occupe);

		Entree[] entree27 = new Entree[1] { SetEntree (false, Hotel_Nom.Safari, Joueur.Couleur.Rouge, Cote.Gauche) };
		Plateau_Jeu.SetCase (27, TypeCase.Construction, new Vector2 (-48.0f, -52.0f),new Hotel[1] {h_Safari}, 336.0f,entree27, Etat_Occupation.Non_Occupe);

		Entree entree28_g = SetEntree(false, Hotel_Nom.Safari, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree28_d = SetEntree(false, Hotel_Nom.Etoile, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree28 = new Entree[2] {entree28_g, entree28_d};
		Plateau_Jeu.SetCase (28, TypeCase.Achat, new Vector2 (-50.0f, -36.0f),new Hotel[2] {h_Safari, h_Etoile},0.0f,entree28, Etat_Occupation.Non_Occupe);

		Entree entree29_g = SetEntree(false, Hotel_Nom.Safari, Joueur.Couleur.Rouge, Cote.Gauche);
		Entree entree29_d = SetEntree(false, Hotel_Nom.Etoile, Joueur.Couleur.Rouge, Cote.Droite);
		Entree[] entree29 = new Entree[2] {entree29_g, entree29_d};
		Plateau_Jeu.SetCase (29, TypeCase.Entrees, new Vector2 (-51.0f, -20.0f),new Hotel[2] {h_Safari, h_Etoile}, 345.0f,entree29, Etat_Occupation.Non_Occupe);

		Entree[] entree30 = new Entree[1] { SetEntree (false, Hotel_Nom.Safari, Joueur.Couleur.Rouge, Cote.Gauche) };
		Plateau_Jeu.SetCase (30, TypeCase.Construction, new Vector2 (-60.0f, -10.0f),new Hotel[1] {h_Safari}, 292.6f,entree30, Etat_Occupation.Non_Occupe);
	}

	// Update is called once per frame
	void Update () {
		
	}

	// Set entree
	public static Entree SetEntree(bool hasProprio, Hotel_Nom nomHotel, Joueur.Couleur Proprietaire, Cote cote)
	{
		Entree monEntree = new Entree();
		monEntree.hasProprio = hasProprio;
		monEntree.nomHotel = nomHotel;
		monEntree.Proprietaire = Proprietaire;
		monEntree.cote = cote;
		return monEntree;
	}
				
	// Set the case, angle ref : 0 = Z azis clockwise
	static void SetCase (int numcase, TypeCase typecase, Vector2 centrecase, Hotel[] listehotel, float angle, Entree[] entree, Etat_Occupation etatOccupation)
	{
		Plateau_Jeu.Jeu[numcase].numCase = numcase;
		Plateau_Jeu.Jeu[numcase].typeCase = typecase;
		Plateau_Jeu.Jeu[numcase].Centre_Case = centrecase;
		Plateau_Jeu.Jeu[numcase].hotel = listehotel;
		Plateau_Jeu.Jeu [numcase].angle_case = angle;
		Plateau_Jeu.Jeu [numcase].entree = entree;
		Plateau_Jeu.Jeu [numcase].etatOccupation = etatOccupation;
	}

	// Set the values of Hotel type
	static void SetHotel (Hotel hotel, Hotel_Nom nom, float Terrain, float Achat_obl, float Entre, float Bat_Princ, float[] Ann, int Nb_Ann, float Base_lois, float[] Prix_N, int nb_construction, bool etatproprio, Joueur.Couleur couleur, Batiment[] bats)
	{
		hotel.Hotel_nom = nom;
		hotel.Terrain	= Terrain;
		hotel.Achat_obligatoire	= Achat_obl;
		hotel.Entree = Entre;
		hotel.Batiment_principal	= Bat_Princ;
		hotel.Annexes = Ann;
		hotel.Nb_Annexes = Nb_Ann;
		hotel.Base_Loisirs = Base_lois;
		hotel.Prix_Nuit = Prix_N;
		hotel.Nb_Construction = nb_construction;
		hotel.Proprietaire = etatproprio;
		hotel.CouleurProprietaire = couleur;
		hotel.batiments = bats;
	}

	// Set the values of hotel batiment
	static void SetBatiment(Batiment batiment, Vector2 emplacement, bool isConstruit, GameObject bat3d)
	{
		batiment.emplacement = emplacement;
		batiment.isConstruit = isConstruit;
		batiment.Bat3D = bat3d;
	}

	// Case occupée ?
	public static bool isOccupee(int numcase)
	{
		return Plateau_Jeu.Jeu [numcase].etatOccupation == Etat_Occupation.Non_Occupe ? false : true;
	}

	// Met à jour l'etat d'occupation de la case
	public void SetCaseOccupation(int numcase, Etat_Occupation etatOccupation)
	{
		Plateau_Jeu.Jeu [numcase].etatOccupation = etatOccupation;
	}

	// Création d'une structure d'analyse d'entrees par defaut
	public static StructRechercheCaseEntree ResetAnalyseCaseEntree()
	{
		StructRechercheCaseEntree Retour;
		Retour.isPossible = false;
		Retour.numCaseEntree = 0;
		Retour.prixEntree = 0.0f;
		Retour.entree = SetEntree (false, Hotel_Nom.Boomerang, Joueur.Couleur.Rouge, Cote.Droite);

		return Retour;
	}
}
