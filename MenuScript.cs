﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MenuScript : MonoBehaviour {
	public static bool[] inscriptionJoueurs = new bool[4] {false, false, false, false};
	public InputField TexteRouge;
	public InputField TexteVert;
	public InputField TexteBleu;
	public InputField TexteJaune;
	public Button BoutonRouge;
	public Button BoutonVert;
	public Button BoutonBleu;
	public Button BoutonJaune;
	public static AudioSource Clic;

	public static InputField[] ListeNom;
	public static Button[] ListeBouton;
	private Text texte;
	public static int nb_player;

	// Use this for initialization
	void Start () {
		ListeNom = new InputField[4] {TexteRouge, TexteVert, TexteBleu, TexteJaune};
		ListeBouton = new Button[4] {BoutonRouge, BoutonVert, BoutonBleu, BoutonJaune};
		Clic = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// Charge la scene suivante si au moins 2 joueurs valides
	public void ChargeJeu()
	{
		if (ValidGO())
		{
			SceneManager.LoadScene("Jeu");
		}
	}

	// Verifie si tous les joueurs ont un nom et qu'il y a au moins 2 joueurs
	private bool ValidGO()
	{
		PlayAudio ();
		bool result = true;
		MenuScript.nb_player = 0;
  		for (int i = 0; i < 4; i++) {
			if (ListeNom[i].text != "") {
				MenuScript.nb_player++;
			} 
		}
		result &= (MenuScript.nb_player > 1);
		Joueur.nb_joueurs = MenuScript.nb_player;
		return result;
	}
		
	// Methode appelée quand on clique sur un bouton
	public void ClicCouleur(int couleur)
	{
		PlayAudio ();
		if (MenuScript.inscriptionJoueurs[couleur] == false)
		{
			ColorBlock cb = ListeBouton [couleur].colors;
			cb.normalColor = ListeBouton [couleur].colors.pressedColor;
			cb.highlightedColor = ListeBouton [couleur].colors.pressedColor;
			ListeBouton [couleur].colors = cb;
			MenuScript.inscriptionJoueurs [couleur] = true;
		} else
		{
			ColorBlock cb = ListeBouton [couleur].colors;
			cb.normalColor = Color.white;
			cb.highlightedColor = Color.white;
			ListeBouton [couleur].colors = cb;
			MenuScript.inscriptionJoueurs [couleur] = false;
		}
	}

	// Joue le clip audio du bouton
	public void PlayAudio()
	{
		Clic.Play ();
	}
}
