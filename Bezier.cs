﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bezier : MonoBehaviour
	{
		//Type de saut
		public enum Type_Saut {Bas_Bas, Bas_Haut, Haut_Haut, Haut_Bas}

		// Spline allant du point 0 au point 2, en passant par un point virtuel 1
		public struct Spline
		{
			public Vector2 P0;
			public Vector2 P1;
			public Vector2 P2;
			public Vector2 T0;
			public Vector2 T2;
		}

		public const float Decalage_Vertical = 5.0f;

		public static Spline[] bezier;

		void Start ()
		{
			bezier = new Spline[Plateau_Jeu.NB_CASES];
		}

		void Update()
		{
		}

		public static void SetBezier(int case_courante)
		{
			float Lambda;
			float angle_depart;
			Vector2 pointDepart;

			if (Joueur.joueurs [Joueur.GetJoueurActif ()].premiercoup) {
				pointDepart = new Vector2(JeuScript.InfoVoitures.AllPositionDepart [(int)Joueur.joueurs [Joueur.GetJoueurActif ()].couleur].x,
				JeuScript.InfoVoitures.AllPositionDepart [(int)Joueur.joueurs [Joueur.GetJoueurActif ()].couleur].z);
				angle_depart = 0.0f;
			} else {
				pointDepart = Plateau_Jeu.Jeu [case_courante].Centre_Case;
				angle_depart = Plateau_Jeu.Jeu [case_courante].angle_case;
			}

			bezier [case_courante].P0 = pointDepart;
			bezier [case_courante].P2 = Plateau_Jeu.Jeu [(case_courante+1)%Plateau_Jeu.NB_CASES].Centre_Case;

		bezier [case_courante].T0 = new Vector2 (Mathf.Cos (Mathf.Deg2Rad*(90.0f - angle_depart)),
			Mathf.Sin (Mathf.Deg2Rad *(90.0f - angle_depart)));
		bezier [case_courante].T2 = new Vector2 (Mathf.Cos (Mathf.Deg2Rad*(90.0f - Plateau_Jeu.Jeu [(case_courante+1)%Plateau_Jeu.NB_CASES].angle_case)),
			Mathf.Sin (Mathf.Deg2Rad *(90.0f - Plateau_Jeu.Jeu [(case_courante+1)%Plateau_Jeu.NB_CASES].angle_case)));

		Lambda = Vector2.Dot ((bezier [case_courante].P2 - bezier [case_courante].P0),new Vector2(-bezier [case_courante].T2.y,bezier[case_courante].T2.x)) /
			Vector2.Dot (bezier [case_courante].T0, new Vector2(-bezier [case_courante].T2.y,bezier[case_courante].T2.x));

		bezier [case_courante].P1 = bezier [case_courante].P0 + Lambda * bezier [case_courante].T0;

		}

	public static Vector3[] CalculBezier(float t, int num_case, Type_Saut saut_voiture)
	{
		float angle_depart;
		float angle_courant;
		Vector2 pointDepart;
		if (Joueur.joueurs [Joueur.GetJoueurActif ()].premiercoup) {
			pointDepart = new Vector2(JeuScript.InfoVoitures.AllPositionDepart [(int)Joueur.joueurs [Joueur.GetJoueurActif ()].couleur].x,
				JeuScript.InfoVoitures.AllPositionDepart [(int)Joueur.joueurs [Joueur.GetJoueurActif ()].couleur].z);
			angle_depart = 0.0f;
		} else {
			pointDepart = Plateau_Jeu.Jeu [num_case].Centre_Case;
			angle_depart = Plateau_Jeu.Jeu[num_case].angle_case;
		}
		Vector2 Point_surf = new Vector2 ();
		Vector3[] Point = new Vector3 [2];
		Vector2 TangenteCourante;
		bool isColineaire = (((angle_depart+360.0f)%180.0f) ==(( Plateau_Jeu.Jeu[(num_case+1)%Plateau_Jeu.NB_CASES].angle_case+360.0f)%180.0f));

		if (isColineaire) {
			Point_surf = t * (Plateau_Jeu.Jeu [(num_case + 1) % Plateau_Jeu.NB_CASES].Centre_Case - pointDepart) + pointDepart;
			angle_courant = Mathf.LerpAngle (angle_depart, Plateau_Jeu.Jeu [(num_case + 1) % Plateau_Jeu.NB_CASES].angle_case, t);
		}
		else {
			SetBezier (num_case);
			Point_surf = (1.0f - t) * (1.0f- t) * bezier [num_case].P0 + 2.0f * t * (1.0f - t) * bezier [num_case].P1 + t * t * bezier [num_case].P2;
			TangenteCourante = 2 * (1.0f - t) * (bezier [num_case].P1 - bezier [num_case].P0) + 2.0f * t * (bezier [num_case].P2 - bezier [num_case].P1);
			angle_courant = Mathf.Rad2Deg * Mathf.Atan2(TangenteCourante.x,TangenteCourante.y);
			Point [1] = new Vector3 (0.0f, angle_courant, 0.0f);
		}
		Point[0].x = Point_surf.x;
		switch (saut_voiture) {
		case Type_Saut.Bas_Bas:
			Point[0].y = JeuScript.InfoVoitures.AllPositionDepart [(int)Joueur.joueurs [Joueur.GetJoueurActif ()].couleur].y;
			break;
		case Type_Saut.Haut_Haut:
			Point[0].y = JeuScript.InfoVoitures.AllPositionDepart [(int)Joueur.joueurs [Joueur.GetJoueurActif ()].couleur].y +Decalage_Vertical;
			break;
		case Type_Saut.Bas_Haut:
			Point[0].y = JeuScript.InfoVoitures.AllPositionDepart [(int)Joueur.joueurs [Joueur.GetJoueurActif ()].couleur].y +t * Decalage_Vertical;
			break;
		case Type_Saut.Haut_Bas:
			Point[0].y = JeuScript.InfoVoitures.AllPositionDepart [(int)Joueur.joueurs [Joueur.GetJoueurActif ()].couleur].y -t * Decalage_Vertical;
			break;
		}
		Point[0].z = Point_surf.y;
		return Point;
	}
}

