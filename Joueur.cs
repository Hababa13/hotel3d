﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Joueur : MonoBehaviour
{
	public enum Etat
	{
Actif,
		Inactif,
		Perdu

	}

	public enum Couleur
	{
Rouge,
		Bleu,
		Vert,
		Jaune

	}

	public struct UnJoueur
	{
		public int case_Courante;
		public float argent;
		public Etat etat;
		public Couleur couleur;
		public Plateau_Jeu.Hotel[] ListeHotels;
		public string nomJoueur;
		public bool premiercoup;
		public int num_Case_Next;
	};

	public static UnJoueur[] joueurs;

	public static int nb_joueurs;

	private static string[] couleurjoueurs;

	// Use this for initialization
	void Start ()
	{
		couleurjoueurs = new string[4] { "Rouge", "Vert", "Bleu", "Jaune" };
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public static void InitJoueur (int numJoueur, Couleur couleur, string nomJoueurs, int numCaseNext)
	{
		joueurs [numJoueur].case_Courante = 0;
		joueurs [numJoueur].argent = 10000.0f;
		joueurs [numJoueur].etat = Etat.Inactif;
		joueurs [numJoueur].couleur = couleur;
		joueurs [numJoueur].ListeHotels = null;
		joueurs [numJoueur].nomJoueur = nomJoueurs;
		joueurs [numJoueur].premiercoup = true;
		joueurs [numJoueur].num_Case_Next = numCaseNext;
	}

	public static void SetEtatJoueur (int numJoueur, Etat etat)
	{
		Joueur.joueurs [numJoueur].etat = etat;
	}

	public static int GetJoueurActif ()
	{
		int numero_joueur = 0;
		for (int i = 0; i < Joueur.nb_joueurs; i++)
			if (Joueur.joueurs [i].etat == Etat.Actif)
				numero_joueur = i;
		return numero_joueur;
	}

	public static string GetStringCouleurJoueurActif ()
	{
		return Joueur.couleurjoueurs [GetJoueurActif ()];
	}

	public static Couleur GetCouleurJoueurActif ()
	{
		return Joueur.joueurs [GetJoueurActif ()].couleur;
	}

	public static void SetProchain_Joueur ()
	{
		int nouveau_joueur_actif = GetJoueurActif ();
		SetEtatJoueur (GetJoueurActif (), Etat.Inactif);
		SetEtatJoueur ((++nouveau_joueur_actif) % Joueur.nb_joueurs, Etat.Actif);
	}

	public static void DebutDePartie ()
	{
		// Initialisation du Jeu
		int nb_joueur = 0;
		// Initialisation des joueurs
		Joueur.joueurs = new Joueur.UnJoueur[Joueur.nb_joueurs];
		for (int i = 0; i < 4; i++) {
			if (MenuScript.inscriptionJoueurs [i]) {
				Joueur.InitJoueur (nb_joueur, (Joueur.Couleur)i, MenuScript.ListeNom [i].text, 0);
				nb_joueur++;
			}
		}
	}

	public static bool SetPaiementJoueur (Joueur.Couleur couleur, float somme)
	{
		bool paiement_ok = true;
		if (Joueur.joueurs [(int)couleur].argent >= somme) {
			Joueur.joueurs [(int)couleur].argent -= somme;
		} else {
			paiement_ok = false;
		}
		return paiement_ok;
	}

	public static void SetCreditJoueur (Joueur.Couleur couleur, float somme)
	{
		Joueur.joueurs [(int)couleur].argent += somme;
	}
}
